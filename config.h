/*  (c) Copyright:  2015  Patrn, Confidential Data
**
**  $Workfile:   config.h
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            
**                      
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       14 Oct 2015
**
 *  Revisions:
 *    $Log:   $
 *
 **/

#ifndef _CONFIG_H_
#define _CONFIG_H_

//=============================================================================
// Feature switches
//=============================================================================
//#define FEATURE_DUMP_DATE                 // Dump data
//#define FEATURE_USE_PRINTF                // Global PRINTF switch
  #define FEATURE_LOG_PRINTF_TO_FILE        // Send LOG_printf's to the Log file

//=============================================================================
// I/O usage
//=============================================================================
#define  RPI_IO_BOARD         "PopKo uses 16x2 I2C LCD Display"
#define  LED_R                0
#define  LED_Y                0
#define  LED_G                0
#define  LED_W                0
//
typedef enum RCUKEYS
{
   RCU_NOKEY = 0,
   RCU_POWER,
   RCU_ENTER,
   RCU_UP,
   RCU_DOWN,
   RCU_LEFT,
   RCU_RIGHT,
   RCU_PLAY,
   RCU_STOP,
   //
   NUM_RCU_KEYS
}  RCUKEYS;
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            FALSE
#define  LED(x, y)            
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      2
#define  LCD_ACTUAL_COLS      16
//
// TBD: share with lcdproc and LCDd
// LCD_MODE() should call a function returning the current mode:
//       return(LCD_MODE_OFF)
//
#define  LCD_MODE(m)
#define  LCD_BACKLIGHT(x)
#define  LCD_COLOR(f,b)
#define  LCD_CURSOR(r,c,x)
#define  LCD_DISPLAY(r,c)
#define  LCD_TEXT(r,c,t)
//
//
#define VERSION                     "PopKodi-v1.00-002"
#define DATA_VALID_SIGNATURE        0xDEADBEEF           // Valid signature
#define DATA_VERSION_MAJOR          1
#define DATA_VERSION_MINOR          1
//
#define RPI_ERROR_FILE              "/mnt/rpicache/popkodi.err"
#define RPI_MAP_FILE                "/mnt/rpicache/popkodi.map"
#define RPI_WORK_DIR                "/mnt/rpicache/"
#define RPI_BACKUP_DIR              "/usr/local/share/rpi/"
//
// Target RAM disk for high load file access
//
#define RPI_PUBLIC_LOG_FILE         "/mnt/rpicache/popkodi.log"
#define RPI_PUBLIC_WWW              "/home/public/www/"
#define RPI_PUBLIC_DEFAULT          "/home/public/www/default.html"
#define RPI_SHELL_DIR               "/mnt/rpicache/"
#define RPI_SHELL_FILE              "popkodi.txt"

#endif /* _CONFIG_H_ */
