/*  (c) Copyright:  2015  Patrn.nl, Confidential Data 
**
**  $Workfile:          rpilcd.c
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            Interface to hd44780 16x2 LCD module using a
**                      mcp23017 i2c I/O expander
**  Entry Points:       
**
**
**
**
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       28 Oct 2015
**
 *  Revisions:
 *
 * Note:                I2C driver for Hitachi HD44780 based LCD displays connected via
 *                      the MCP23017 I2C I/O expander. The LCD is operated in its 4 bit-mode 
 *                      to be connected to the 8 bit-port of a single MCP23017.
 *
 *                        HD44780                 MCP-23017
 *                                            +--------------+
 *                         BL_B o-------------o B0        A7 o----BL_G
 *                         D7   o-------------o B1        A6 o----BL_R
 *                         D6   o-------------o B2        A5 o    
 *                         D5   o-------------o B3        A4 o----L-pin3
 *                         D4   o-------------o B4        A3 o----U-pin3
 *                         EN   o-------------o B5        A2 o----D-pin4
 *                         R/W  o-------------o B6        A1 o----R-pin3
 *                         RS   o-------------o B7        A0 o----S-pin4
 *                         V+   o-------------o Vdd     INTA o    
 *                         Gnd  o-------------o Vss     INTB o   
 *                                            o         /RST o   
 *                                    --------o SCL       A2 o----Gnd
 *                                    --------o SCA       A1 o----Gnd
 *                                            o           A0 o----Gnd
 *                                            +--------------+
 *
 *                      Connections:
 *                      =========================================================================
 *                      HD44780              MCP23017     
 *                      Green LCD backlight  GPA7         
 *                      Red LCD backlight    GPA6         
 *                      Left key             GPA4         
 *                      Up key               GPA3         
 *                      Down key             GPA2         
 *                      Right key            GPA1         
 *                      Select key           GPA0         
 *                      RS (4)               GPB7         0=Instr - 1=Data
 *                      RW (5)               GPB6         0=Read  - 1=Write
 *                      EN (6)               GPB5         
 *                      D7 (14)              GPB4         
 *                      D6 (13)              GPB3         
 *                      D5 (12)              GPB2         
 *                      D4 (11)              GPB1         
 *                      Blue LCD backlight   GPB0         
 *
 *                      Configuration:
 *                      =========================================================================
 *                      device   = /dev/i2c-1      the device file of the i2c bus
 *                      port     = 0x20            the i2c address of the i2c port expander
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>

#include "config.h"
#include "echotype.h"
#include "log.h"
#include "globals.h"
#include "rpilcd.h"

//#define USE_PRINTF
#include "printf.h"


#define  FEATURE_I2C_DELAY
#define  I2C_RD_DELAY         0              // Read  Delay in uSec
#define  I2C_INIT_DELAY       5000           // Init  Delay in uSec
#define  I2C_CLR_DELAY        10000          // Clear screen Delay in uSec
#define  I2C_WR_DELAY         100            // Write Delay in uSec
//
#ifdef   FEATURE_I2C_DELAY
 #define I2C_DELAY(x)             usleep(x)
#else
 #define I2C_DELAY(x)
#endif   //FEATURE_I2C_DELAY
//
//       #define  FEATURE_SHOW_HD44780
#ifdef   FEATURE_SHOW_HD44780
 static void lcd_ShowHd44780Reg   (u_int8, u_int8);
 #define LCD_SHOWHD44780REG(m,d)  lcd_ShowHd44780Reg(m,d)
#else
 #define LCD_SHOWHD44780REG(m,d)
#endif   //FEATURE_SHOW_HD44780
//
// MCP23017 I2C IO expander 
//
#define I2C_MCP23017_ADDR     0x20
#define I2C_ADDR_MASK         0x7f
//
// MCP23017 registers (IOCON.BANK=0)
//
#define MCP23017_IODIRA       0x00
#define MCP23017_IODIRB       0x01
#define MCP23017_IOCON        0x0A
#define MCP23017_GPPUA        0x0C
#define MCP23017_GPPUB        0x0D
#define MCP23017_GPIOA        0x12
#define MCP23017_GPIOB        0x13
//
// IOCON bits
//
#define BANK_BIT              (1 << 7)
#define MIRROR_BIT            (1 << 6)
#define SEQOP_BIT             (1 << 5)
#define DISSLW_BIT            (1 << 4)
#define HAEN_BIT              (1 << 3)
#define ODR_BIT               (1 << 2)
#define INTPOL_BIT            (1 << 1)
//
// GPIOA BITS
//
#define G_BIT                 (1 << 7)
#define R_BIT                 (1 << 6)
#define L_KEY_BIT             (1 << 4)
#define U_KEY_BIT             (1 << 3)
#define D_KEY_BIT             (1 << 2)
#define R_KEY_BIT             (1 << 1)
#define S_KEY_BIT             (1 << 0)
//
// GPIOB BITS
//
#define RS_BIT                (1 << 7)
#define RW_BIT                (1 << 6)
#define EN_BIT                (1 << 5)
#define D4_BIT                (1 << 4)
#define D5_BIT                (1 << 3)
#define D6_BIT                (1 << 2)
#define D7_BIT                (1 << 1)
#define B_BIT                 (1 << 0)
//
// HD44780 Modes
//
#define HD_IW                 (0x00<<6)      // Instruction register Read
#define HD_IR                 (0x01<<6)      //                      Write
#define HD_DW                 (0x02<<6)      // Data        register Read 
#define HD_DR                 (0x03<<6)      //                      Write
//
//
// The hd44780 has:
//    o 80 bytes Display data RAM
//    o 2 lines
//    o 16 chars per line
//
#define  HD_DDRAM_BYTES       80    // DDRAM
#define  HD_DDRAM_MASK        0x7f  // DDRAM address mask
#define  HD_DDRAM_ROWS        2     // Physical : 2 x 40 chars
#define  HD_DDRAM_COLS        40    //
#define  HD_DDRAM_LINE_0      0x00  // DDRAM address line 0, col 0
#define  HD_DDRAM_LINE_1      0x40  //               line 1, col 0
//

//
// HD44780 Instructions
//                                     // RS R/W DB:7 6 5 4 3 2 1 0
#define HD_CLEAR              0x01     //  0  0     0 0 0 0 0 0 0 1  Clears display and DDRAM=0
#define HD_HOME               0x02     //  0  0     0 0 0 0 0 0 1 -  Shift=0; DDRAM=0
#define HD_EMS                0x04     //  0  0     0 0 0 0 0 1 I S  I=1/D=0; Set cursor move dir
#define HD_OOC                0x08     //  0  0     0 0 0 0 1 D C B  On/Off Control Display-Cursor-Blinking
#define HD_CDS                0x10     //  0  0     0 0 0 1 S R - -  Cursor move; cursor shift
#define HD_FUNC               0x20     //  0  0     0 0 1 D N F - -  Datalength-Nr Lines-Font
#define HD_CGRAM              0x40     //  0  0     0 1 x x x x x x  CGRAM Address
#define HD_DDRAM              0x80     //  0  0     1 x x x x x x x  DDRAM Address
#define HD_BUSYBIT            0x80     //  0  1     B a a a a a a a  Busy flag-address counter
//
#define HD_EMS_I              0x02     // Auto Increment=1; Auto Decrement=0
#define HD_EMS_S              0x01     // Include display shift
//
#define HD_OOC_B              0x01     // OnOff control: 1=Blinking cursor
#define HD_OOC_C              0x02     // OnOff control: 1=Cursor on
#define HD_OOC_D              0x04     // OnOff control: 1=Display
//
#define HD_FUNC_F             0x04     // Func Set Font   : 0=5x8dots 1=5x10dots;
#define HD_FUNC_N             0x08     // Func Set NrLines: 0=1line   1=2lines
#define HD_FUNC_DL            0x10     // Func Set DataLen: 0=4bits   1=8bits
//
// Local data
//
static bool    fI2cOkee       = FALSE;
static int     iLcdFd         = -1;
static int     iLcdMode       = MODE_DP_OFF;
static int     iLcdAddr       = 0;
static int     iI2cDelay      = I2C_INIT_DELAY;
//
static const u_int8  ubLutLcd[16] =
{
   0x00, 0x08, 0x04, 0x0c, 0x02, 0x0a, 0x06, 0x0e,
   0x01, 0x09, 0x05, 0x0d, 0x03, 0x0b, 0x07, 0x0f
};

//
// Local prototypes
//
static bool lcd_Backlight        (LMODE);
static bool lcd_FillScreen       (u_int8);
static bool lcd_InitLcd          (void);
static bool lcd_ReadExp          (u_int8, u_int8 *);
static bool lcd_ReadLcd          (u_int8, u_int8 *);
static bool lcd_SetDisplayAddress(u_int8, u_int8);
static bool lcd_WriteExp         (u_int8, u_int8);
static bool lcd_WriteLcd         (u_int8, u_int8);

/* ======   Local Functions separator ===========================================
void ___main_functions(){}
==============================================================================*/

//
//  Function:  LCD_ClearScreen
//  Purpose:   Clear the LCD Screen
//
//  Parms:     
//  Returns:   FALSE on error
//
bool LCD_ClearScreen()
{
   bool fCc;

   if(!fI2cOkee) return(FALSE);
   iLcdAddr = 0;
   fCc = lcd_WriteLcd(HD_IW, HD_CLEAR);
   I2C_DELAY(I2C_CLR_DELAY);
   return(fCc);
}

//
//  Function:  LCD_Close
//  Purpose:   Close LCD 
//
//  Parms:     
//  Returns:   FALSE on error
//
bool LCD_Close()
{
   if(iLcdFd > 0) 
   {
      LCD_Mode(LCD_MODE_BACKLIGHT_OFF);
      LCD_Mode(LCD_MODE_DISPLAY_OFF);
      iLcdFd = 0;
   }
   //
   fI2cOkee=FALSE;
   return(TRUE);
}

//
//  Function:  LCD_FillScreen
//  Purpose:   Fill the LCD Screen with data
//
//  Parms:     Data
//  Returns:   FALSE on error
//
bool LCD_FillScreen(u_int8 ubData)
{
   if(!fI2cOkee) return(FALSE);
   return( lcd_FillScreen(ubData) );
}

//
//  Function:  LCD_Mode
//  Purpose:   Setup correct LCD Mode
//
//  Parms:     Mode
//  Returns:   FALSE on error
//
bool LCD_Mode(LMODE tMode)
{
   if(!fI2cOkee) return(FALSE);
   //
   PRINTF("LCD_Mode():..." CRLF);
   //
   switch(tMode)
   {
      case LCD_MODE_DISPLAY_OFF:
         //
         // Turn displays OFF (if not already)
         //
         if( (iLcdMode & MODE_DP_ON) ) 
         {
            iLcdMode &= ~MODE_DP_ON;
            return(lcd_WriteLcd(HD_IW, HD_OOC));
         }
         else PRINTF("LCD_Mode():Display already OFF" CRLF);
         break;

      case LCD_MODE_DISPLAY_ON:
         //
         // Turn displays ON (if not already)
         //
         if(!(iLcdMode & MODE_DP_ON) ) 
         {
            iLcdMode |= MODE_DP_ON;
            return(lcd_WriteLcd(HD_IW, HD_OOC|HD_OOC_D));
         }
         else PRINTF("LCD_Mode():Display already ON" CRLF);
         break;

      case LCD_MODE_BACKLIGHT_ON:
         if(!(iLcdMode & MODE_BL_ON) ) return(lcd_Backlight(LCD_MODE_BACKLIGHT_ON ));
         else                          PRINTF("LCD_Mode():BL already ON" CRLF);
         break;

      case LCD_MODE_BACKLIGHT_OFF:
         if( (iLcdMode & MODE_BL_ON) ) return(lcd_Backlight(LCD_MODE_BACKLIGHT_OFF));
         else                          PRINTF("LCD_Mode():BL already OFF" CRLF);
         break;

      default:
         break;
   }
   PRINTF("LCD_Mode():OK" CRLF);
   return(TRUE);
}

//
//  Function:  LCD_Open
//  Purpose:   Initialize the LCD module
//
//  Parms:
//  Returns:   FALSE on error
//  Note:      
//
//
bool LCD_Open()
{
   fI2cOkee=FALSE;
   //
   PRINTF("LCD_Open():..." CRLF);
   //
   if(!lcd_InitLcd()) return(FALSE);
   //
   PRINTF("LCD_Open():OK" CRLF);
   fI2cOkee=TRUE;
   return(fI2cOkee);
}

//
//  Function:  LCD_Read
//  Purpose:   Read data from the LCD module 
//
//  Parms:     Row, Col, dataptr, length
//  Returns:   FALSE on error
//
bool LCD_Read(u_int8 ubRow, u_int8 ubCol, u_int8 *pubData, int iLen)
{
   bool fCc=TRUE;

   if(!fI2cOkee) return(FALSE);
   //
   //PRINTF3("LCD_Read():L=%d C=%d bytes=%d" CRLF, ubRow, ubCol, iLen);
   if(!lcd_SetDisplayAddress(ubRow, ubCol)) return(FALSE);
   //
   while(iLen--)
   {
      if(!lcd_ReadLcd(HD_DR, pubData)) return(FALSE);
      pubData++;
   }
   //
   return(fCc);
}

//
//  Function:  LCD_Write
//  Purpose:   Write data to the LCD module 
//
//  Parms:     Line, Col, dataptr, length
//  Returns:   FALSE on error
//
bool LCD_Write(u_int8 ubRow, u_int8 ubCol, u_int8 *pubData, int iLen)
{
   if(!fI2cOkee) return(FALSE);
   //
   if(!lcd_SetDisplayAddress(ubRow, ubCol)) return(FALSE);
   //
   //PRINTF3("LCD_Write():L=%d C=%d bytes=%d" CRLF, ubRow, ubCol, iLen);
   //
   while(iLen--)
   {
      if(*pubData == 0)                    return(TRUE);
      if(!lcd_WriteLcd(HD_DW, *pubData++)) return(FALSE);
   }
   return(TRUE);
}

/* ======   Local Functions separator ===========================================
void ___local_functions(){}
==============================================================================*/

//
//  Function:  lcd_Backlight
//  Purpose:   Turn BL on/off
//
//  Parms:     Mode
//  Returns:   TRUE if OKee
//
//  Note:      Turn display backlight on or off. 
//             For the Pi Plate this means a read/modify/write cycle is required, 
//             as the bits are shared with other signals on the same byte.
//
static bool lcd_Backlight(LMODE tMode)
{
   u_int8   ubGpioA=0;
   //pwjh u_int8   ubGpioB=0;
   
   //
   // Read the RG bits
   //
   if( !lcd_ReadExp(MCP23017_GPIOA, &ubGpioA) ) return(FALSE);
   PRINTF1("lcd_Backlight():PortA=0x%x" CRLF, ubGpioA);
   //
   //pwjh if( !lcd_ReadExp(MCP23017_GPIOB, &ubGpioB) ) return(FALSE);
   //pwjh PRINTF1("lcd_Backlight():PortB=0x%x" CRLF, ubGpioB);
   //
   // Set or clear the RGB bits. Backlight is ON if the bits are CLEAR
   //
   switch(tMode)
   {
      default:
         return(FALSE);

      case LCD_MODE_BACKLIGHT_ON:
         ubGpioA  &= ~(R_BIT | G_BIT);
         //pwjh ubGpioB  &= ~(B_BIT);
         iLcdMode |= MODE_BL_ON;
         break;

      case LCD_MODE_BACKLIGHT_OFF:
         ubGpioA  |= (R_BIT | G_BIT);
         //pwjh ubGpioB  |= (B_BIT);
         iLcdMode &= ~MODE_BL_ON;
         break;
   }
   //
   // Write the RGB bits
   //
   if( !lcd_WriteExp(MCP23017_GPIOA, ubGpioA) ) return(FALSE);
   //pwjh if( !lcd_WriteExp(MCP23017_GPIOB, ubGpioB) ) return(FALSE);
   //
   return(TRUE);
}


//
//  Function:  lcd_FillScreen
//  Purpose:   Fill the whole LCD screen
//
//  Parms:     Data
//  Returns:   TRUE if OKee
//
static bool lcd_FillScreen(u_int8 ubData)
{
   PRINTF1("lcd_FillScreen(): Pat=0x%x-- NOT YET" CRLF, ubData);
   return(TRUE);
}

//
//  Function:  lcd_InitLcd
//  Purpose:   Init the hd44780 LCD
//
//  Parms:     
//  Returns:   TRUE if OKee
//
static bool lcd_InitLcd(void)
{
   //
   // Setup I2C devices
   //
   if( (iLcdFd = wiringPiI2CSetup(I2C_MCP23017_ADDR)) < 0) return(FALSE);
   I2C_DELAY(iI2cDelay);
   //
   // IOCON - All settings as default
   //          IOCON.BANK=0   :A/B regs are paired
   //               .SEQOP=0  :AddrPtr auto-incr
   // IODIRA - keys as input, all other as output
   // IODIRB - LCD control/data bus signals as outputs
   // GPPUA - Pullups for key switch inputs
   // GPPUB - Pullups disabled
   //
   if( !lcd_WriteExp(MCP23017_IOCON,  (u_int8) 0x00) )                                                                              return(FALSE);
   if( !lcd_WriteExp(MCP23017_IODIRA, (u_int8) (L_KEY_BIT | U_KEY_BIT | D_KEY_BIT | R_KEY_BIT | S_KEY_BIT) & ~(G_BIT | R_BIT)) )    return(FALSE);
   if( !lcd_WriteExp(MCP23017_IODIRB, (u_int8) 0x00) )                                                                              return(FALSE);
   if( !lcd_WriteExp(MCP23017_GPPUA,  (u_int8) (L_KEY_BIT | U_KEY_BIT | D_KEY_BIT | R_KEY_BIT | S_KEY_BIT) & ~(G_BIT | R_BIT)) )    return(FALSE);
   if( !lcd_WriteExp(MCP23017_GPPUB,  (u_int8) 0x00) )                                                                              return(FALSE);
   //
   //   +---+---+---+---+---+---+---+---+
   //   |I/D|R/W|EN |D4 |D5 |D6 |D7 |BLb|
   //   +---+---+---+---+---+---+---+---+
   //
   // The hd44780 comes up from reset as:
   //    o Display clear
   //    o Function set: 
   //          DL=1  (8bit)        <-------------- 8bit on Power-up !!
   //          N=0   (1-line)
   //          F=0   (5x8dots)
   //    o Display OnOff control:
   //          D=0   (Display off)
   //          C=0   (Cursor off)
   //          B=0   (Blinking off)
   //    o Entry mode set:
   //          I/D=1 (Increment by 1)
   //          S=0   (No shift)
   //
   //
   // =============================================================================================================
   // This driver will do TWO write operations always since it assumes 4bit mode (MS-nibble + LS-nibble) !!
   // =============================================================================================================
   //
   // Assume HD44780 is in 8 bit mode (upon POWER-UP)
   // ==============================================
   //
   //
   //         WriteLcd(x)                      PortB                     HD44780                                        Instruction
   //                               +---+---+---+---+---+---+---+---+   +---+---+ +---+---+---+---+---+---+---+---+
   //                               |I/D|R/W|EN |D4 |D5 |D6 |D7 |BLb|   |I/D|R/W| | D7| D6| D5| D4| D3| D2| D1| D0|
   //                               +---+---+---+---+---+---+---+---+   +---+---+ +---+---+---+---+---+---+---+---+
   //
   //  0x33   0 0 1 1 . 0 0 1 1       0   0   1   1   1   0   0   1       0   0     0   0   1   1   x   x   x   x       FUNC 8bit
   //                                 0   0   1   1   1   0   0   1       0   0     0   0   1   1   x   x   x   x       FUNC 8bit
   //  0x32   0 0 1 1 . 0 0 1 0       0   0   1   1   1   0   0   1       0   0     0   0   1   1   x   x   x   x       FUNC 8bit
   //                                 0   0   1   0   1   0   0   1       0   0     0   0   1   0   x   x   x   x       FUNC 4bit
   //
   // Assume HD44780 is already in 4 bit mode
   // ==============================================
   //
   //
   //         WriteLcd                         PortB                     HD44780  
   //                               +---+---+---+---+---+---+---+---+   +---+---+ +---+---+---+---+---+---+---+---+
   //                               |I/D|R/W|EN |D4 |D5 |D6 |D7 |BLb|   |I/D|R/W| | D7| D6| D5| D4| D3| D2| D1| D0|
   //                               +---+---+---+---+---+---+---+---+   +---+---+ +---+---+---+---+---+---+---+---+
   //
   //  0x33   0 0 1 1 . 0 0 1 1       0   0   1   1   1   0   0   1       0   0     0   0   1   1                       
   //                                 0   0   1   1   1   0   0   1       0   0                     0   0   1   1
   //  0x32   0 0 1 1 . 0 0 1 0       0   0   1   1   1   0   0   1       0   0     0   0   1   1  
   //                                 0   0   1   0   1   0   0   1       0   0                     0   0   1   0  
   //
   //
   //
   //
   //
   if(!lcd_WriteLcd(HD_IW, 0x33))                           return(FALSE);       // 
   if(!lcd_WriteLcd(HD_IW, 0x32))                           return(FALSE);       // 
   
   if(!lcd_WriteLcd(HD_IW, HD_FUNC|HD_FUNC_N))              return(FALSE);       // Function       : N:1=2 lines
   //
   if(!lcd_WriteLcd(HD_IW, HD_OOC))                         return(FALSE);       // Display Ctl    : D:1=Display;C:1=Cursor;B:1=Blink
   if(!lcd_WriteLcd(HD_IW, HD_EMS|HD_EMS_I))                return(FALSE);       // Entry mode     : I:1=Auto increment
   //if(!lcd_WriteLcd(HD_IW, HD_DDRAM))                       return(FALSE);       // DDRAM          :
   if(!lcd_WriteLcd(HD_IW, HD_CLEAR))                       return(FALSE);       // Display        : Clear
   //if(!lcd_WriteLcd(HD_IW, HD_HOME))                        return(FALSE);       // Display        : Home
   //
   iI2cDelay = I2C_WR_DELAY;
   //
   return(TRUE);
}

//
//  Function:  lcd_ReadExp
//  Purpose:   Read data from one of the I/O expander registers
//
//  Parms:     Reg, dataptr
//  Returns:   TRUE if OKee
//
static bool lcd_ReadExp(u_int8 ubReg, u_int8 *pubData)
{
   int   iData;

   //
   // Read the register
   //
   if( (iData = wiringPiI2CReadReg8(iLcdFd, ubReg)) < 0) return(FALSE);
   I2C_DELAY(I2C_RD_DELAY);
   //
   PRINTF2("lcd_ReadExp():Reg[%d]=0x%x" CRLF, ubReg, iData);
   *pubData = (u_int8) iData;
   return(TRUE);
}

//
//  Function:  lcd_ReadLcd
//  Purpose:   Read data from the HD44780
//
//  Parms:     Instruction/Data mode, Valueptr
//  Returns:   FALSE on error
//
static bool lcd_ReadLcd(u_int8 ubMode, u_int8 *pubVal)
{
   u_int8   ubNibbleLsb, ubNibbleMsb;
   u_int8   ubWriteVal=ubMode;
   u_int8   ubReadVal;

   //
   // Read the IR or DR from the HD44780 (as 2x4 nibbles)
   // 
   //  HD44780     MCP23017
   //  =========  =====================================
   //    BL_B o----o B0  :Backlight Blue: Preserve !
   //    D7   o----o B1  :
   //    D6   o----o B2  :
   //    D5   o----o B3  :
   //    D4   o----o B4  :
   //    EN   o----o B5  :0=Disable 1=Enable
   //    R/W  o----o B6  :0=Write   1=Read
   //    RS   o----o B7  :0=Instr   1=Data
   // 
   if(iLcdMode & MODE_BL_ON) ubWriteVal |= B_BIT;
   //
   // Read both nibbles from the HD44780
   //
   if( !lcd_WriteExp(MCP23017_GPIOB, ubWriteVal |  EN_BIT)) return(FALSE);
   if( !lcd_WriteExp(MCP23017_GPIOB, ubWriteVal & ~EN_BIT)) return(FALSE);
   if( !lcd_ReadExp(MCP23017_GPIOB, &ubReadVal) )           return(FALSE);
   LCD_SHOWHD44780REG(ubMode, ubWriteVal);
   ubNibbleMsb = ubLutLcd[(ubReadVal>>1) & 0x0f];
   PRINTF2("lcd_ReadLcd(0x%x):MS=0x%x" CRLF, ubMode, ubNibbleMsb);
   //
   if( !lcd_WriteExp(MCP23017_GPIOB, ubWriteVal |  EN_BIT)) return(FALSE);
   if( !lcd_WriteExp(MCP23017_GPIOB, ubWriteVal & ~EN_BIT)) return(FALSE);
   if( !lcd_ReadExp(MCP23017_GPIOB, &ubReadVal) )           return(FALSE);
   LCD_SHOWHD44780REG(ubMode, ubWriteVal);
   ubNibbleLsb = ubLutLcd[(ubReadVal>>1) & 0x0f];
   PRINTF2("lcd_ReadLcd(0x%x):LS=0x%x" CRLF, ubMode, ubNibbleLsb);
   //
   ubReadVal = ((ubNibbleMsb & 0x0f)<<4) | (ubNibbleLsb & 0x0f);
   PRINTF2("lcd_ReadLcd(0x%x):read=0x%x" CRLF, ubMode, ubReadVal);
   *pubVal = ubReadVal;
   return(TRUE);
}

//
//  Function:  lcd_SetDisplayAddress
//  Purpose:   Set the display addr of the HD44780
//
//  Parms:     row, col
//  Returns:   FALSE on error
//
static bool lcd_SetDisplayAddress(u_int8 ubRow, u_int8 ubCol)
{
   iLcdAddr = (int) (ubRow * HD_DDRAM_COLS) + ubCol;
   if(iLcdAddr >= HD_DDRAM_BYTES) return(FALSE);

   switch(ubRow)
   {
      case 0:
         iLcdAddr = (int) (HD_DDRAM_LINE_0 + ubCol);
         break;

      case 1:
         iLcdAddr = (int) (HD_DDRAM_LINE_1 + ubCol);
         break;

      default:
         return(FALSE);
         break;
   }
   //
   // Set address
   //
   if( !lcd_WriteLcd(HD_IW, (u_int8)(HD_DDRAM|(iLcdAddr & HD_DDRAM_MASK))) ) return(FALSE);
   //
   return(TRUE);
}

//
//  Function:  lcd_WriteExp
//  Purpose:   Write data to one of the I/O expander registers
//
//  Parms:     Reg, data
//  Returns:   -1 on error
//
static bool lcd_WriteExp(u_int8 ubReg, u_int8 ubData)
{
   if( wiringPiI2CWriteReg8(iLcdFd, ubReg, (int)ubData) < 0) return(FALSE);
   I2C_DELAY(iI2cDelay);
   PRINTF2("lcd_WriteExp():Reg[%d]=0x%x" CRLF, ubReg, ubData);
   return(TRUE);
}

//
//  Function:  lcd_WriteLcd
//  Purpose:   Write data to the HD44780
//
//  Parms:     Instruction/Data mode, Value to write
//  Returns:   -1 on error
//
static bool lcd_WriteLcd(u_int8 ubMode, u_int8 ubVal)
{
   u_int8   ubWriteVal;
   int      iIdx;

   //PRINTF3("lcd_WriteLcd(0x%x):0x%x (%c)" CRLF, ubMode, ubVal, ubVal);
   //
   // Write to the IR or DR from the HD44780 (as 2x4 nibbles)
   // 
   //  HD44780     MCP23017
   //  =========  =====================================
   //    BL_B o----o B0  :Backlight Blue: Preserve !
   //    D7   o----o B1  :
   //    D6   o----o B2  :
   //    D5   o----o B3  :
   //    D4   o----o B4  :
   //    EN   o----o B5  :0=Disable       1=Enable
   //    R/W  o----o B6  :0=Write         1=Read
   //    RS   o----o B7  :0=Instruction   1=Data
   // 
   for (iIdx=0; iIdx<2; iIdx++) 
   {
      //
      // Adjust for 4-bit data pins
      //
      if(iIdx) ubWriteVal = (u_int8) (ubLutLcd[ubVal & 0x0f] << 1);     // LS nibble (last)
      else     ubWriteVal = (u_int8) (ubLutLcd[ubVal >> 4]   << 1);     // MS Nibble (first)
      //
      // Backlight-Blue bit if backlight is on
      //
      if(iLcdMode & MODE_BL_ON) ubWriteVal |= B_BIT;
      //
      if( (lcd_WriteExp(MCP23017_GPIOB, ubMode | (ubWriteVal |  EN_BIT)) ) < 0) return(FALSE);
      if( (lcd_WriteExp(MCP23017_GPIOB, ubMode | (ubWriteVal & ~EN_BIT)) ) < 0) return(FALSE);
      //
      LCD_SHOWHD44780REG(ubMode, ubWriteVal);
   }
   return(TRUE);
}

/* ======   Local Functions separator ===========================================
void ___test_functions(){}
==============================================================================*/

#ifdef   FEATURE_SHOW_HD44780
//
//  Function:  lcd_ShowHd44780Reg
//  Purpose:   Show the bits send to the HD44780
//
//  Parms:     Mode, data
//  Returns:   
//
static void lcd_ShowHd44780Reg(u_int8 ubMode, u_int8 ubPortB)
{
   static int     iLcdNibble     = 0;
   static char    cRegImage[32];
   //
   int            i;
   char          *pcRegImage;

   if(iLcdNibble) pcRegImage=&cRegImage[8];
   else           pcRegImage=&cRegImage[0];

   for(i=0; i<4; i++)
   {
      ubPortB >>= 1;
      if(ubPortB & 1)   *pcRegImage++ = '1';
      else              *pcRegImage++ = '0';
                        *pcRegImage++ = ' ';
   }
   *pcRegImage++ = 0;
   //
   if(iLcdNibble) 
   {
      switch(ubMode)
      {
         default:
               PRINTF( "lcd_ShowHd44780Data: Unknown command" CRLF);
            break;

         case HD_IR:
               PRINTF( "lcd_ShowHd44780Data: RS RW  [7 6 5 4 3 2 1 0]" CRLF);
               PRINTF1("lcd_ShowHd44780Data: 0  1   [%s]" CRLF, cRegImage);
            break;

         case HD_IW:
               PRINTF( "lcd_ShowHd44780Data: RS RW  [7 6 5 4 3 2 1 0]" CRLF);
               PRINTF1("lcd_ShowHd44780Data: 0  0   [%s]" CRLF, cRegImage);
            break;

         case HD_DW:
               PRINTF( "lcd_ShowHd44780Data: RS RW  [7 6 5 4 3 2 1 0]" CRLF);
               PRINTF1("lcd_ShowHd44780Data: 1  0   [%s]" CRLF, cRegImage);
            break;

         case HD_DR:
               PRINTF( "lcd_ShowHd44780Data: RS RW  [7 6 5 4 3 2 1 0]" CRLF);
               PRINTF1("lcd_ShowHd44780Data: 1  1   [%s]" CRLF, cRegImage);
            break;
      }
   }
   //
   iLcdNibble ^= 1;
}
#endif   //FEATURE_SHOW_HD44780
