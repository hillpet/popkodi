/*  (c) Copyright:  2012  Patrn, Confidential Data
 *
 *  $Workfile:          dynpage.h
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            Header file for HTTP web server helper files
 *
 *
 *  Entry Points:       
 *
 *
 *
 *
 *
 *  Compiler/Assembler: 
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
 *
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _DYNPAGE_H_
#define _DYNPAGE_H_

#include "netfiles.h"

typedef bool (*PFNCL)(NETCL *);

typedef struct DYNPAGE
{
   char    *pcFilename;
   PFNCL    pfHandler;
}  DYNPAGE;

typedef struct RPINFO
{
   char    *pcTrigger;           // Source trigger text
   char    *pcTitle;             // Virtual display title text
   u_int8   ubRow, ubCol;        // Virtual display coords
   int      iOffset;             // Source text skip offset
   int      iLength;             // Virtual display max text length
}  RPINFO;
//
// Global prototypes
//
bool  HTTP_PageIsDynamic         (NETCL *);
bool  HTTP_SendDynamicPage       (NETCL *);
bool  HTTP_BuildGeneric          (NETCL *l, char *, ...);
bool  HTTP_PageSend              (NETCL *, char *);
void  HTTP_SendString            (NETCL *, char *);
void  HTTP_SendData              (NETCL *, char *, int);

//
// Generic HLMT Page build calls
//
bool  HTTP_BuildRespondHeader    (NETCL *);
bool  HTTP_BuildRespondHeaderBad (NETCL *);
bool  HTTP_BuildRespondHeaderJson(NETCL *);
bool  HTTP_BuildStart            (NETCL *);
bool  HTTP_BuildLineBreaks       (NETCL *, int);
bool  HTTP_BuildDivStart         (NETCL *);
bool  HTTP_BuildDivEnd           (NETCL *);
bool  HTTP_BuildEnd              (NETCL *);
//
bool  HTTP_BuildTableStart       (NETCL *, char *, u_int16, u_int16, u_int16);
bool  HTTP_BuildTableRowStart    (NETCL *, u_int16);
bool  HTTP_BuildTableRowEnd      (NETCL *);
bool  HTTP_BuildTableColumnText  (NETCL *, char *, u_int16);
bool  HTTP_BuildTableColumnLink  (NETCL *, char *, u_int16, char *);
bool  HTTP_BuildTableColumnNumber(NETCL *, u_int16, u_int16);
bool  HTTP_BuildTableEnd         (NETCL *);


#endif   //_DYNPAGE_H_
