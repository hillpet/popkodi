/*  (c) Copyright:  2014  Patrn.nl, Confidential Data 
**
**  $Workfile:          rpircu
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            Interface PifaceCad IR RCU functions
**
**  Entry Points:       
**
**
**
**
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       20 Feb 2014
**
 *  Revisions:
 *    $Log:   $
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <lirc/lirc_client.h>

#include "config.h"
#include "echotype.h"
#include "netfiles.h"
#include "log.h"
#include "globals.h"
#include "rpircu.h"

//#define USE_PRINTF
#include "printf.h"

static struct lirc_config *pstRcuConfig=NULL;
static bool                fLircRunning=TRUE;
//
// Local prototypes
//
static void rcu_Exit                ();
static int  rcu_LookupRcuKey        (char *);
static bool rcu_Process             ();
static void rcu_ReceiveIrKeys       ();
static void rcu_ReceiveSignalTerm   (int);
static void rcu_ReceiveSignalInt    (int);
static void rcu_SendSignalToServer  (int);
static bool rcu_SignalRegister      (sigset_t *);

//
//  Function:   RCU_Close
//  Purpose:    Close PifaceCad RCU interface
//
//  Parms:      
//  Returns:    
//
void RCU_Close(void)
{
}

//
//  Function:   RCU_Open
//  Purpose:    Open PifaceCad RCU interface
//
//  Parms:      
//  Returns:    
//
bool RCU_Open(void)
{
   return( rcu_Process() );
}


/*------  Local functions separator ------------------------------------
__Local_functions_(){};
----------------------------------------------------------------------------*/
// 
//  Function    : rcu_Exit
//  Description : Exit this thread
// 
//  Parameters  : 
//  Returns     : 
// 
// 
static void rcu_Exit()
{
   //
   // Termination signal has been receiver: clean up and exit
   //
   if(pstRcuConfig)
   {
      lirc_freeconfig(pstRcuConfig);
      lirc_deinit();
      pstRcuConfig = NULL;
      PRINTF("rcu_Exit(): Term signal received: exit." CRLF);
   }
   fLircRunning = FALSE;
   PRINTF("rcu_Exit(): LIRC thread now exiting" CRLF);
}

// 
//  Function    : rcu_LookupRcuKey
//  Description : Lookup the RCU key for signalling to the main thread
// 
//  Parameters  : Ascii RCU key
//  Returns     : Key enum or RCU_NOKEY
// 
//  Note        : The RCU IR Keynames are defined in the lirc config file: /etc/lirc/lircrc
//                The actual remote is defined in the RCU config file:     /etc/lirc/lircd.conf
// 
static int rcu_LookupRcuKey(char *pcKey)
{
   const RCULUT stRcuLookup[] =
   {
      { "Power",      RCU_POWER         },
      { "Enter",      RCU_ENTER         },
      { "Up",         RCU_UP            },
      { "Down",       RCU_DOWN          },
      { "Left",       RCU_LEFT          },
      { "Right",      RCU_RIGHT         },
      { "Play",       RCU_PLAY          },
      { "Stop",       RCU_STOP          }
   };
   const int iNumRcuLookup = sizeof(stRcuLookup)/sizeof(RCULUT);

   int   x, iCc=RCU_NOKEY;

   for(x=0; x<iNumRcuLookup;x++)
   {
      if(strcmp((char *)stRcuLookup[x].pcIrKey, pcKey) == 0)
      {
         iCc = stRcuLookup[x].iRcuKey;
         break;
      }
   }
   return(iCc);
}

// 
//  Function    : rcu_Process
//  Description : Split this thread and run the lirc function client in a 
//                sepatate thread
// 
//  Parameters  : 
//  Returns     : TRUE if OKee
// 
// 
static bool rcu_Process()
{
   bool     fCc=TRUE;
   pid_t    tPid=-1;
   RPIMAP  *pstMap;

   //==========================================================================
   // Split process : 
   //       Parent thread returns with tPid of the EXEC thread
   //       LIRC   thread returns with tPid=0
   //
   tPid = fork();
   //==========================================================================
   switch(tPid)
   {
      case 0:
         //
         // LIRC child
         //
         rcu_ReceiveIrKeys();
         _exit(254);
         break;

      case -1:
         // Error
         fCc = FALSE;
         break;
            
      default:
         //
         // This is the parent thread which started the LIRC process
         //
         PRINTF1("rcu_Process(): LIRC thread now running: pid=%d" CRLF, tPid);
         pstMap = GLOBAL_GetMapping();
         pstMap->G_tLircPid = tPid;
         break;
   }
   return(fCc);
}

// 
//  Function    : rcu_ReceiveIrKeys
//  Description : Start receiving the RCU IR keys using the LIRC socket (localhost:8765)
// 
//  Parameters  : 
//  Returns     : Not at all, killed by the main thread throug <G_tLircPid>
// 
// 
static void rcu_ReceiveIrKeys()
{
   sigset_t tBlockset;
   int      iFd, iCc, iFpKey;
   char    *pcCode=NULL;
   char    *pcConfig=NULL;

   if( rcu_SignalRegister(&tBlockset) )
   {
      PRINTF("rcu_ReceiveIrKeys(): This is the LIRC thread running....." CRLF);
      //
      // Setup RCU IR
      //
      if( (iFd = lirc_init("popkodi", 1)) == -1)
      {
         //
         // Problems setting up LIRC
         //
         LOG_Report(errno, "RCU", "rcu_ReceiveIrKeys(): Problems opening LIRC");
         PRINTF1("rcu_ReceiveIrKeys(): Problems opening LIRC (%d)" CRLF, errno);
      }
      else
      {
         if(lirc_readconfig(NULL, &pstRcuConfig, NULL) != 0)
         {
            LOG_Report(errno, "RCU", "rcu_ReceiveIrKeys(): Problems opening RCU Config");
            PRINTF1("rcu_ReceiveIrKeys(): Problems opening RCU Config (%d)" CRLF, errno);
         }
         else
         {
            while(fLircRunning)
            {
               //
               // This is the main RCU IR loop, reading IR keys from the LIRC socket
               //
               iCc = lirc_nextcode(&pcCode);
               if(iCc == 0)
               {
                  if(pcCode)
                  {
                     PRINTF1("rcu_ReceiveIrKeys(): LIRC=%s", pcCode);
                     do
                     {
                        if( ((iCc = lirc_code2char(pstRcuConfig, pcCode, &pcConfig)) == 0) && pcConfig != NULL)
                        {
                           //
                           // The RCU Lirc key has been defined in the /etc/lirc/lircrc configfile
                           //
                           iFpKey = rcu_LookupRcuKey(pcConfig);
                           //
                           rcu_SendSignalToServer(iFpKey);
                           PRINTF2("rcu_ReceiveIrKeys(): CONF=[%s]=%d" CRLF, pcConfig, iFpKey);
                        }
                     }
                     while(pcConfig);
                     free(pcCode);
                  }
                  else
                  {
                     LOG_Report(errno, "RCU", "rcu_ReceiveIrKeys(): Problems reading LIRC");
                     PRINTF1("rcu_ReceiveIrKeys(): Problems reading LIRC (%d)" CRLF, errno);
                  }
               }
               else
               {
                  LOG_Report(errno, "RCU", "rcu_ReceiveIrKeys(): Problems reading LIRC nextcode");
                  PRINTF1("rcu_ReceiveIrKeys(): Problems reading LIRC nextcode (%d)" CRLF, errno);
               }
            }
         }
      }
   }
}

//
//  Function:   rcu_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rcu_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "RCU", "rcu_ReceiveSignalTerm()");
   PRINTF1("rcu_ReceiveSignalTerm(): signal=%d" CRLF, iSignal);
   rcu_Exit();
}

//
//  Function:   rcu_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rcu_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "RCU", "rcu_ReceiveSignalInt()");
   PRINTF1("rcu_ReceiveSignalInt(): signal=%d" CRLF, iSignal);
   rcu_Exit();
}

// 
//  Function    : rcu_SendSignalToServer
//  Description : Signal the HTTP-Server
// 
//  Parameters  : Data
//  Returns     : 
// 
//  Note        : Uses MMAP shared memory
// 
static void rcu_SendSignalToServer(int iData)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   pstMap->G_iFrontPanelKey = iData;
   //
   // Notify the server we have a RCU IR key
   //
   PRINTF2("rcu_SendSignalToServer(): SRVR(%d), Data=%d" CRLF, pstMap->G_tSrvrPid, iData);
   kill(pstMap->G_tSrvrPid, SIGUSR1);
}

//
//  Function:   rcu_SignalRegister
//  Purpose:    Register SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool rcu_SignalRegister(sigset_t *ptBlockset)
{
   bool fCC = TRUE;

   // CTL-X handler
   if( signal(SIGTERM, &rcu_ReceiveSignalTerm) == SIG_ERR)
   {
      LOG_Report(errno, "RCU", "rcu_SignalRegister(): SIGTERM Error");
      fCC = FALSE;
   }
   // Ctl-C handler
   if( signal(SIGINT, &rcu_ReceiveSignalInt) == SIG_ERR)
   {
      LOG_Report(errno, "RCU", "rcu_SignalRegister(): SIGINT Error");
      fCC = FALSE;
   }
   if(fCC)
   {
      //
      // Setup the SIGxxx events
      //
      sigemptyset(ptBlockset);
      sigaddset(ptBlockset, SIGTERM);
      sigaddset(ptBlockset, SIGINT);
   }
   return(fCC);
}



