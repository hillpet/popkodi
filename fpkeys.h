/*  (c) Copyright:  2015  Patrn.nl, Confidential Data
**
**  $Workfile:          fpkeys.h
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            fpkeys header file
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       2 Nov 2015
**
 *  Revisions:
 *    $Log:   $
 *
 *
**/

#ifndef _FPKEYS_H_
#define _FPKEYS_H_

#define TIME_PER_SECS      1
//
#define VLCD_ROW_TAND      0
#define VLCD_COL_TAND      16
//
#define VLCD_ROW_TIME      0
#define VLCD_COL_TIME      19
#define VLCD_ROW_DATE      1
#define VLCD_COL_DATE      17
//
#define VLCD_ROW_MISC      0
#define VLCD_COL_MISC      0
//
#define VLCD_ROW_MISC1     0
#define VLCD_COL_MISC1     0
//   
#define VLCD_ROW_MISC2     1
#define VLCD_COL_MISC2     0
//
typedef enum FPSTATE
{
   FPS_NONE       = 0,   
   FPS_STBY,
   FPS_KODI,
   FPS_DISP,
   FPS_EXIT,
   //
   NUM_FP_STATES
}  FPSTATE;

typedef enum FPMODE
{
   FPM_OFF        = 0,
   FPM_TIME,
   FPM_CMD_EXIT,
   FPM_LOGO,
   FPM_CMD_BOOT,
   FPM_CMD_OFF,
   //
   NUM_FP_MODES
}  FPMODE;

typedef FPSTATE(*PKEYH)(FPSTATE, u_int8);

typedef struct FPKEYH
{
   PKEYH    pfHandler;
   FPSTATE  tNewState;
}  FPKEYH;



//
// Global prototypes
//
void     FP_KeyExit        (void);
FPSTATE  FP_KeyHandler     (FPSTATE, u_int8);
void     FP_KeyInit        (void);
#endif  /*_FPKEYS_H_ */

