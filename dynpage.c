/*  (c) Copyright:  2012  Patrn, Confidential Data  
 *
 *  $Workfile:          dynpage.cpp
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            Dynamic HTTP page for Raspberry pi webserver
 *
 *
 *  Entry Points:       HTTP_PageIsDynamic()
 *                      HTTP_SendDynamicPage()
 *                      HTTP_PageSend()
 *                      HTTP_SendString()
 *                      HTTP_SendData()
 *                      HTTP_BuildGeneric()
 *
 *  Compiler/Assembler: gnu gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Jul 2012
 *
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "echotype.h"
#include "dynpage.h"
#include "httppage.h"
#include "log.h"
#include "raspjson.h"
#include "misc_api.h"
#include "globals.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static bool    http_DynPageEmpty             (NETCL *);
static bool    http_DynPageDefault           (NETCL *);
static bool    http_DynPageShell             (NETCL *);
static bool    http_DynPageLog               (NETCL *);
static bool    http_DynPageScreen            (NETCL *);
static bool    http_DynPageScreenJson        (NETCL *);
static bool    http_DynPageExit              (NETCL *);
//             
static bool    http_ExPageGeneric            (NETCL *, char *);
//             
static bool    http_ExPageStart              (NETCL *, char *);
//
static char   *http_ExtractPageName          (NETCL *);
static bool    http_SendTextFile             (NETCL *, char *);
static bool    http_Xmt                      (NETCL *, char);

//
// These are the supported dynamic HTTP pages
//
static const DYNPAGE stDynamicPages[] =
{
   { "",                http_DynPageEmpty          },
   { "info.html",       http_DynPageDefault        },
   { "cmd.html",        http_DynPageShell          },
   { "log.html",        http_DynPageLog            },
   { "screen.html",     http_DynPageScreen         },
   { "exit.html",       http_DynPageExit           },
   //
   { "screen.json",     http_DynPageScreenJson     },
   {  0,                0                          }
};

//
//
//
static const RPINFO stVirtualInfo[] = 
{  // trigger              title                row   col   offs  length
   { "top -",              "",                   2,    0,    6,   80    },
   { "MemTotal",           NULL,                 3,    0,    0,   80    },
   { "MemFree",            NULL,                 4,    0,    0,   80    },
   //
   { NULL,                 NULL,                 0,    0,    0,   0     }
};
//
// Misc http header strings 
//
static char pcWebPageTitle[]           = "PATRN Embedded Software Services";
static char pcWebPageShell[]           = "RaspberryPi$ %s";
//
static char pcWebPageLineBreak[]       = "<br/>";
static char pcWebPagePreStart[]        = "<div align=\"left\"><pre>";
static char pcWebPagePreEnd[]          = "</pre></div> ";
static char pcWebPageShellBad[]        = "<p>Cmd shell: bad request (errno=%d)</p>" NEWLINE;
//
static char pcWebPageFontStart[]       = "<font face=\"%s\">";
static char pcWebPageFontEnd[]         = "</font>";
//
// HTTP Response:
//=============================
//    HTTP Response header
//    NEWLINE
//    Optional Message body
//    NEWLINE NEWLINE
//
static char pcHttpResponseHeader[]     = "HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
static char pcHttpResponseHeaderJson[] = "HTTP/1.0 200 OK" NEWLINE "Content-Type=application/json" NEWLINE NEWLINE;
static char pcHttpResponseHeaderBad[]  = "HTTP/1.0 400 Bad Request" NEWLINE NEWLINE;

//
// Generic HTTP header
//
static char pcWebPageStart[] =
{  
   "<!doctype html public \"-//w3c//dtd html 4.01//en\"" NEWLINE \
   "<html>" NEWLINE \
      "<head>" NEWLINE \
         "<style type=\"text/css\">" NEWLINE \
            "thead {font-family:\"Tahoma\";font-size:\"100%%\";color:green}" NEWLINE \
            "tbody {font-family:\"Courier new\";font-size:\"25%%\";color:blue}" NEWLINE \
         "</style>" NEWLINE \
         "<meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1252\">" NEWLINE \
         "<title>%s</title>" NEWLINE \
      "</head>" NEWLINE \
      "<body style=\"text-align:center\">" NEWLINE
};

static char pcWebPageDefault[] =
{
   "<hr/>" NEWLINE \
   "<h2>PATRN ESS</h2>" NEWLINE \
   "<h3>Raspberry Pi HTTP server</h3>" NEWLINE \
   "<hr/>" NEWLINE
};

static char pcWebPageEmpty[] =
{
   "<br/>" NEWLINE
   "Welcome to another PATRN mini HTTP server." NEWLINE \
   "<a href=\"http://www.patrn.nl\" target=\"_blank\">Patrn.nl</a>" NEWLINE \
   "<br/>" NEWLINE
};


static char pcWebExitPage[] =
{
   "<br/>" NEWLINE \
   "<br/>" NEWLINE \
   "Exiting PATRN HTTP server. Good bye !" NEWLINE \
   "<br/>" NEWLINE \
   "<br/>" NEWLINE
};

//
// HTTP table start: Needs border, width(%), colspan, headerstring
//
static char pcWebPageTableStart[] =
{
   "<table align=center border=%d width=\"%d%%\">" NEWLINE \
      "<body style=\"text-align:left\">" NEWLINE
      "<thead>" NEWLINE \
      "<th colspan=%d>%s</th>" NEWLINE
};

//
// HTTP table end
//
static char pcWebPageTableEnd[] =
{
   "</tbody></table>" NEWLINE
};

//
// HTTP table row: Needs row height
//
static char pcWebPageTableRowStart[] =
{
   "<tr height=%d>" NEWLINE
};

//
// HTTP table row end
//
static char pcWebPageTableRowEnd[] =
{
   "</tr>" NEWLINE
};

//
// HTTP table cel data: Needs [cell width,] data
//
static char pcWebPageCellDataSimple[] = 
{
   "<td>%s</td>" NEWLINE
};
static char pcWebPageCellDataExtended[] =
{
   "<td width=\"%d%%\">%s</td>" NEWLINE NEWLINE
};

static char* pcWebPageCellData[] =
{
   pcWebPageCellDataSimple,
   pcWebPageCellDataExtended
};

//
// HTTP table cel link data : Needs [cell width,] link, data
//
static char pcWebPageCellDataLinkSimple[] =
{
   "<td><a href=\"%s\">%s</td>" NEWLINE
};
static char pcWebPageCellDataLinkExtended[] =
{
   "<td width=\"%d%%\"><a href=\"%s\">%s</td>" NEWLINE
};

static char * pcWebPageCellDataLink[] =
{
   pcWebPageCellDataLinkSimple,
   pcWebPageCellDataLinkExtended
};

//
// HTTP table cel numeric data : Needs [cell width,] data
//
static char pcWebPageCellDataNumberSimple[] =
{
   "<td>%d</td>" NEWLINE
};
static char pcWebPageCellDataNumberExtended[] =
{
   "<td width=\"%d%%\">%d</td>" NEWLINE
};

static char * pcWebPageCellDataNumber[] =
{
   pcWebPageCellDataNumberSimple,
   pcWebPageCellDataNumberExtended
};

//
// Generic HTTP trailer
//
static char pcWebPageEnd[] =
{
   "</body></html>" NEWLINE NEWLINE 
};

//
// HTTP table cel link data : Needs [cell width,] link, data
//
static char pcWebPageText[] =
{
   "[%s]" NEWLINE
};

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function    : HTTP_PageIsDynamic
// Description : Check if the requested page is dynamic or (file)static
//
// Parameters  : Net client struct
// Returns     : TRUE if requested page is dynamic (and needs construction)
//
bool HTTP_PageIsDynamic(NETCL *pstCl)
{
   bool           fCc=FALSE;
   const DYNPAGE *pstDynPage=stDynamicPages;
   char          *pcFilename;
   char          *pcPagename;

   pcPagename = http_ExtractPageName(pstCl);

   if ( pcPagename == NULL)   return(FALSE);
   if (*pcPagename == '/')    pcPagename++;
   if (*pcPagename == 0)      return(TRUE);

   while(TRUE)
   {
      pcFilename  = pstDynPage->pcFilename;
      if(pcFilename == NULL) break;
      //
      if(strcmp(pcFilename, pcPagename) == 0)
      {
         //
         // This request is a dynamic HTTP page
         //
         fCc = TRUE;
         break;
      }
      else
      {
         pstDynPage++;
      }
   }
   return(fCc);
}

/*
 * Function    : HTTP_BuildGeneric
 * Description : Build a generic http page
 *
 * Parameters  : Client, string^, parameters
 * Returns     : TRUE if OKee
 *
 */
bool HTTP_BuildGeneric(NETCL *pstCl, char *pcPage, ...)
{
   va_list  tParms;
   bool     fParsing=1;
   bool     fCc=TRUE;
   int      i=0, iVal;
   int32    lVal;
   char    *pcData, cChar, cBuffer[20];
   
   va_start(tParms, pcPage);
   //
   while(fParsing)
   {
      cChar = pcPage[i];
      switch(cChar)
      {
         case 0:
            fParsing = 0;
            break;
            
         case '%':
            i++;
            cChar = pcPage[i];
            switch(cChar)
            {
               case '%':
                  http_Xmt(pstCl, cChar);
                  break;
               
               case 'd':
                  iVal = va_arg(tParms, int);
                  ES_sprintf (cBuffer, "%d", iVal);
                  HTTP_SendString(pstCl, cBuffer);
                  break;
               
               case 'x':
                  iVal = va_arg(tParms, int);
                  ES_sprintf (cBuffer, "%x", iVal);
                  HTTP_SendString(pstCl, cBuffer);
                  break;
               
               case 'l':
                  lVal = va_arg(tParms, int32);
                  ES_sprintf (cBuffer, "%ld", lVal);
                  HTTP_SendString(pstCl, cBuffer);
                  break;
               
               case 's':
                  pcData = va_arg(tParms, char *);
                  HTTP_SendString(pstCl, pcData);
                  break;
               
               case 'S':
                  pcData = va_arg(tParms, char *);
                  HTTP_SendString(pstCl, pcData);
                  break;
               
               default:
                  fCc = FALSE;
                  break;
            }
            break;
            
         default:
            http_Xmt(pstCl, cChar);
            break;
      }
      i++;
   }
   va_end(tParms);
   
   return(fCc);
}

// 
// Function    : HTTP_PageSend
// Description : Dynamically create and send a http web page
// 
// Parameters  : NETCL *, Page name
// Returns     : TRUE if OKee
// 
bool HTTP_PageSend(NETCL *pstCl, char *pcPagename)
{
   bool           fCC=FALSE;
   const DYNPAGE *pstDynPage=stDynamicPages;
   char          *pcFilename;
   PFNCL          pfAddr;

   if ( pcPagename == NULL)   return(FALSE);
   if (*pcPagename == '/')    pcPagename++;

   while(TRUE)
   {
      pcFilename = pstDynPage->pcFilename;
      if(pcFilename == 0) break;
      if(strcmp(pcFilename, pcPagename) == 0)
      {
         //
         // This request is a dynamic HTTP page: go create it
         // 
         pfAddr = (PFNCL) pstDynPage->pfHandler;
         if(pfAddr)
         {
            fCC = pfAddr(pstCl);
         }
         break;
      }
      else
      {
         pstDynPage++;
      }
   }
   return(fCC);
}

// 
// Function    : HTTP_SendDynamicPage
// Description : Dynamically create and send a http web page
// 
// Parameters  : Client
// Returns     : TRUE if OKee
// 
bool HTTP_SendDynamicPage(NETCL *pstCl)
{
   bool        fCC;
   char       *pcPagename;

   pcPagename = http_ExtractPageName(pstCl);
   fCC        = HTTP_PageSend(pstCl, pcPagename);

   return(fCC);
}

//
// Function    : HTTP_SendData
// Description : Put out Client buffer
//
// Parameters  : Client
// Returns     : 
//
void HTTP_SendData(NETCL *pstCl, char *pcData, int iLen)
{
   int   iIdx=0;

   while(iIdx<iLen)
   {
      http_Xmt(pstCl, pcData[iIdx]);
      iIdx++;
   }
}

/*
 * Function    : HTTP_SendString
 * Description : Display http string
 *
 * Parameters  : Client, string^
 * Returns     : 
 *
 */
void HTTP_SendString(NETCL *pstCl, char *pcData)
{
   char    cChar;
   int     i=0;

   while(1)
   {
      cChar = pcData[i];
      //
      if(cChar)
      {
         http_Xmt(pstCl, cChar);
         i++;
      }
      else break;
   }
}

/*------  Local functions separator ------------------------------------
__BUILD_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function    : HTTP_BuildStart
// Description : Put out HTTP page start 
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildStart(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML header, start tag, title etc
   //
   fOKee &= http_ExPageStart(pstCl,   pcWebPageTitle);
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageDefault);
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function    : HTTP_BuildRespondHeader
// Description : Put out HTTP response header
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildRespondHeader(NETCL *pstCl)
{
   return( http_ExPageGeneric(pstCl, pcHttpResponseHeader) );
}

//
// Function    : HTTP_BuildRespondHeaderBad
// Description : Put out HTTP response header Bad Request
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildRespondHeaderBad(NETCL *pstCl)
{
   return( http_ExPageGeneric(pstCl, pcHttpResponseHeaderBad) );
}

//
// Function    : HTTP_BuildRespondHeaderJson
// Description : Put out HTTP page start Json
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildRespondHeaderJson(NETCL *pstCl)
{
   return( http_ExPageGeneric(pstCl, pcHttpResponseHeaderJson) );
}

//
// Function    : HTTP_BuildLineBreaks
// Description : Put out HTTP linebreaks
//
// Parameters  : Client, number of newlines
// Returns     : True if okee
//
bool HTTP_BuildLineBreaks(NETCL *pstCl, int iNr)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML header, start tag, title etc
   //
   while(iNr--)   fOKee &= http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function    : HTTP_BuildDivStart
// Description : Put out HTTP page div/section start 
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildDivStart(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML div start tag
   //
   fOKee &= http_ExPageGeneric(pstCl, pcWebPagePreStart);
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function    : HTTP_BuildDivEnd
// Description : Put out HTTP page div/section end
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildDivEnd(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML div end tag
   //
   fOKee &= http_ExPageGeneric(pstCl, pcWebPagePreEnd);
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function    : HTTP_BuildEnd
// Description : Put out HTTP page end
//
// Parameters  : Client
// Returns     : True if okee
//
bool HTTP_BuildEnd(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML end
   //
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   fOKee &= http_ExPageGeneric(pstCl, pcWebPageEnd);
   return(fOKee);
}

/*------  Local functions separator ------------------------------------
__TABLE_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : HTTP_BuildTableStart
 * Description : Start a http web page table
 *
 * Parameters  : Client, page header, border with, table width, number of columns
 * Returns     :
 *
 */
bool  HTTP_BuildTableStart(NETCL *pstCl, char *pcPage, u_int16 usBorder, u_int16 usWidth, u_int16 usCols)
{
   bool  fOKee;
   
   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageTableStart, usBorder, usWidth, usCols, pcPage);

   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableRowStart
 * Description : Starts a row in a table
 *
 * Parameters  : Client, row height
 * Returns     :
 *
 */
bool  HTTP_BuildTableRowStart(NETCL *pstCl,  u_int16 usHeight)
{
   bool  fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageTableRowStart, usHeight);
   
   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableRowEnd
 * Description : Ends a row
 *
 * Parameters  : Client
 * Returns     :
 *
 */
bool  HTTP_BuildTableRowEnd(NETCL *pstCl)
{
   bool fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageTableRowEnd);
   
   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableColumnText
 * Description : Start a generic column
 *
 * Parameters  : Client, page struct, column text, column width
 * Returns     :
 *
 */
bool  HTTP_BuildTableColumnText(NETCL *pstCl, char *pcText, u_int16 usWidth)
{
   bool     fOKee;

   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellData[1], usWidth, pcText);
   else        fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellData[0], pcText);

   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableColumnLink
 * Description : Start a link with a hyperlink
 *
 * Parameters  : Client, page struct, column text, width, link
 * Returns     :
 *
 */
bool  HTTP_BuildTableColumnLink(NETCL *pstCl, char *pcText, u_int16 usWidth, char *pcLink)
{
   bool  fOKee;

   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellDataLink[1], usWidth, pcLink, pcText);
   else        fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellDataLink[0], pcLink, pcText);

   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableColumnNumber
 * Description : Start a column with a number
 *
 * Parameters  : Client, page struct, number, width
 * Returns     :
 *
 */
bool  HTTP_BuildTableColumnNumber(NETCL *pstCl, u_int16 usNr, u_int16 usWidth)
{
   bool     fOKee;
   
   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellDataNumber[1], usWidth, usNr);
   else        fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageCellDataNumber[0], usNr);

   return(fOKee);
}

/*
 * Function    : HTTP_BuildTableEnd
 * Description : End a http web page table
 *
 * Parameters  : Client
 * Returns     :
 *
 */
bool  HTTP_BuildTableEnd(NETCL *pstCl)
{
   bool  fOKee;
   
   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageTableEnd);

   return(fOKee);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : http_DynPageEmpty
 * Description : Dynamically created empty page
 *
 * Parameters  : Client
 * Returns     : 1 if OKee
 *
 */
static bool http_DynPageEmpty(NETCL *pstCl)
{
   if(!http_SendTextFile(pstCl, RPI_PUBLIC_DEFAULT) )
   {
      //
      // Put out a default reply: the HTML header, start tag and the rest
      //         
      http_ExPageGeneric(pstCl, pcHttpResponseHeader);
      //
      http_ExPageStart(pstCl,   pcWebPageTitle);
      http_ExPageGeneric(pstCl, pcWebPageDefault);
      http_ExPageGeneric(pstCl, pcWebPageEmpty);
      http_ExPageGeneric(pstCl, pcWebPageEnd);
   }
   return(1);
}

/*
 * Function    : http_DynPageDefault
 * Description : Dynamically created default page
 *
 * Parameters  : Client
 * Returns     : 1 if OKee
 *
 */
static bool http_DynPageDefault(NETCL *pstCl)
{
   //
   // Put out the HTML header, start tag and the rest
   //         
   http_ExPageGeneric(pstCl, pcHttpResponseHeader);
   //
   http_ExPageStart(pstCl,   pcWebPageTitle);
   http_ExPageGeneric(pstCl, pcWebPageDefault);
   http_ExPageGeneric(pstCl, pcWebPageEnd);
   
   return(1);
}

/*
 * Function    : http_DynPageShell
 * Description : Dynamically created system page
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 * Note        : This call acts as a router to Linux command shell
 *                GET /cmd?ps&-a&-x blahblah
 *
 *                 --> system("ps -a -x ><dir>/<file>")
 *                     return <file> to caller as text output from the command
 *
 */
static bool http_DynPageShell(NETCL *pstCl)
{
   bool  fCc=TRUE;
   int   iCc;
   char *pcShell;
   char *pcCmd;
   char *pcTemp;

   pcShell = safemalloc(MAX_CMD_LEN);
   pcCmd   = safemalloc(MAX_CMD_LEN);
   //
   // Collect command and parameters, execute command and return its reply 
   //
   pcTemp = HTTP_CollectGetParameter(pstCl);
   //if(pcTemp) PRINTF1("http_DynPageShell():P=<%s>" CRLF, pcTemp);
   //else       PRINTF("http_DynPageShell():No Parms" CRLF);
   if(pcTemp)
   {
      //
      // We have a Shell command: collect the whole string
      //
      ES_sprintf(pcShell, "%s", pcTemp);
      do
      {
         ES_sprintf(pcCmd, "%s", pcShell);
         pcTemp = HTTP_CollectGetParameter(pstCl);
         if(pcTemp) ES_sprintf(pcShell, "%s %s", pcCmd, pcTemp);
      }
      while(pcTemp);
      //
      ES_sprintf(pcCmd, "%s", pcShell);
      ES_sprintf(pcShell, "%s >%s%s", pcCmd, RPI_SHELL_DIR, RPI_SHELL_FILE);
      PRINTF1("http_DynPageShell():%s" CRLF, pcShell);
      //
      // Execute the Shell command
      //
      LOG_Report(0, "DYN", "Run system call [%s]", pcShell);
      iCc = system(pcShell);
      // 
      //
      // Put out the HTML header, start tag and the rest
      //         
      http_ExPageGeneric(pstCl, pcHttpResponseHeader);
      http_ExPageStart(pstCl,   pcWebPageTitle);
      http_ExPageGeneric(pstCl, pcWebPageLineBreak);
      http_ExPageGeneric(pstCl, pcWebPageDefault);
      //
      //
      if(iCc < 0) 
      {
         LOG_Report(errno, "DYN", "ERROR: Running system call [%s]", pcShell);
         PRINTF2("http_DynPageShell(): Error completion %d (errno=%d)" CRLF, iCc, errno);
         HTTP_BuildGeneric(pstCl, (char *)pcWebPageShellBad, errno);
         fCc=FALSE;
      }
      //pwjh HTTP_BuildGeneric(pstCl,     pcWebPageFontStart, "Courier New");
      http_ExPageGeneric(pstCl, pcWebPagePreStart);
      //
      // Send the shell results to the client pcWebPageShell
      //
      HTTP_BuildGeneric(pstCl, (char *)pcWebPageShell, pcShell);
      http_ExPageGeneric(pstCl, pcWebPageLineBreak);
      http_ExPageGeneric(pstCl, pcWebPageLineBreak);
      ES_sprintf(pcCmd, "%s%s", RPI_SHELL_DIR, RPI_SHELL_FILE);
      http_SendTextFile(pstCl, pcCmd);
      http_ExPageGeneric(pstCl, pcWebPagePreEnd);
      //pwjh http_ExPageGeneric(pstCl, pcWebPageFontEnd);
      http_ExPageGeneric(pstCl, pcWebPageEnd);
   }
   else
   {
      http_ExPageGeneric(pstCl, pcHttpResponseHeaderBad);
      http_ExPageStart(pstCl,   pcWebPageTitle);
      http_ExPageGeneric(pstCl, pcWebPageEnd);
      fCc=FALSE;
   }
   //
   safefree(pcShell);
   safefree(pcCmd);
   PRINTF("http_DynPageShell():Done" CRLF);
   return(fCc);
}

/*
 * Function    : http_DynPageScreen
 * Description : Dynamically created system page
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 * Note        : This call acts as a router to Linux command shell
 *                GET /cmd?ps&-a&-x blahblah
 *
 *                 --> system("ps -a -x ><dir>/<file>")
 *                     return <file> to caller as text output from the command
 *
 */
static bool http_DynPageScreen(NETCL *pstCl)
{
   bool     fCc=TRUE;
   int      iRow;
   char    *pcTemp;
   char    *pcVirtual;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   VLCD    *pstLcd=&pstMap->G_stVirtLcd;

   pcTemp = safemalloc(LCD_VIRTUAL_COLS+1);
   //
   // Put out the HTML header, start tag and the rest
   //         
   http_ExPageGeneric(pstCl, pcHttpResponseHeader);
   http_ExPageStart(pstCl,   pcWebPageTitle);
   http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   http_ExPageGeneric(pstCl, pcWebPageDefault);
   //
   HTTP_BuildGeneric(pstCl,  pcWebPageFontStart, "Courier New");
   http_ExPageGeneric(pstCl, pcWebPagePreStart);
   //
   pcVirtual = (char *)pstLcd->pcVirtLcd;
   for(iRow=0; iRow<LCD_VIRTUAL_ROWS; iRow++)
   {
      memcpy(pcTemp, pcVirtual, LCD_VIRTUAL_COLS);
      pcTemp[LCD_VIRTUAL_COLS] = 0;
      HTTP_BuildGeneric(pstCl, pcWebPageText, pcTemp);
      pcVirtual += LCD_VIRTUAL_COLS;
   }
   http_ExPageGeneric(pstCl, pcWebPagePreEnd);
   http_ExPageGeneric(pstCl, pcWebPageFontEnd);
   http_ExPageGeneric(pstCl, pcWebPageEnd);
   //
   safefree(pcTemp);
   PRINTF("http_DynPageScreen():Done" CRLF);
   return(fCc);
}

/*
 * Function    : http_DynPageLog
 * Description : Dynamically created system page to show the log file
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 */
static bool http_DynPageLog(NETCL *pstCl)
{
   // 
   // Put out the HTML header, start tag and the rest
   //         
   http_ExPageGeneric(pstCl, pcHttpResponseHeader);
   http_ExPageStart(pstCl,   pcWebPageTitle);
   http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   http_ExPageGeneric(pstCl, pcWebPageDefault);
   //
   http_ExPageGeneric(pstCl, pcWebPagePreStart);
   http_ExPageGeneric(pstCl, pcWebPageLineBreak);
   http_SendTextFile(pstCl,  RPI_PUBLIC_LOG_FILE);
   http_ExPageGeneric(pstCl, pcWebPagePreEnd);
   http_ExPageGeneric(pstCl, pcWebPageEnd);
   //
   PRINTF("http_DynPageLog():OKee" CRLF);
   return(1);
}

/*
 * Function    : http_DynPageScreenJson
 * Description : Dynamically created virtual screen using JSON reply
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 */
static bool http_DynPageScreenJson(NETCL *pstCl)
{
   bool        fCc=TRUE;
   int         iRow;
   RPIJSON    *pstObj=NULL;
   char       *pcTemp;
   char       *pcVirtual;
   RPIMAP     *pstMap=GLOBAL_GetMapping();
   VLCD       *pstLcd=&pstMap->G_stVirtLcd;

   pcTemp = safemalloc(LCD_VIRTUAL_COLS+1);
   //
   // Put out the contents of the virtual screen 
   //
   pstObj = JSON_InsertParameter(pstObj, "Command", "VirtualScreen", JE_CURLB|JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Output", "", JE_SQRB|JE_CRLF);
   //
   pcVirtual = (char *)pstLcd->pcVirtLcd;
   for(iRow=0; iRow<LCD_VIRTUAL_ROWS; iRow++)
   {
      memcpy(pcTemp, pcVirtual, LCD_VIRTUAL_COLS);
      pcTemp[LCD_VIRTUAL_COLS] = 0;
      pstObj = JSON_InsertValue(pstObj, pcTemp, JE_TEXT|JE_COMMA|JE_CRLF);
      pcVirtual += LCD_VIRTUAL_COLS;
   }
   pstObj = JSON_TerminateArray(pstObj);
   pstObj = JSON_TerminateObject(pstObj);
   fCc    = JSON_RespondObject(pstCl, pstObj);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstObj);
   //
   PRINTF("http_DynPageScreenJson():Done" CRLF);
   safefree(pcTemp);
   return(fCc);
}

/*
 * Function    : http_DynPageExit
 * Description : Dynamically created system page to exit the server
 *
 * Parameters  : Client
 * Returns     : TRUE if OKee
 *
 * Note        : This call acts as a gracefull exit from the server to the Linux command shell
 *
 */
static bool http_DynPageExit(NETCL *pstCl)
{
   //
   // Put out the HTML header, start tag and the rest
   //         
   http_ExPageGeneric(pstCl, pcHttpResponseHeader);
   //
   http_ExPageStart(pstCl,   pcWebPageTitle);
   http_ExPageGeneric(pstCl, pcWebPageDefault);
   http_ExPageGeneric(pstCl, pcWebExitPage);
   http_ExPageGeneric(pstCl, pcWebPageEnd);
   //
   PRINTF("http_DynPageExit():OKee" CRLF);
   LOG_Report(0, "DYN", "Exit request from browser");
   //
   // Request our own exit
   //
   raise(SIGTERM);
   return(TRUE);
}

/*------  Local functions separator ------------------------------------
__HTTP_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : http_ExPageGeneric
 * Description : Send out generic data to the http client
 *
 * Parameters  : Client
 * Returns     :
 *
 */
static bool  http_ExPageGeneric(NETCL *pstCl, char *pcData)
{
   bool fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcData);

   return(fOKee);
}

/*
 * Function    : http_ExPageStart
 * Description : Starts a web page
 *
 * Parameters  : Client
 * Returns     :
 *
 */
static bool  http_ExPageStart(NETCL *pstCl, char *pcTitle)
{
   bool fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, (char *)pcWebPageStart, pcTitle);

   return(fOKee);
}

/*
 * Function    : http_ExtractPageName
 * Description : Remove HTTP specific chars 
 *
 * Parameters  : Net client struct
 * Returns     : Filename ptr
 *
 */
static char *http_ExtractPageName(NETCL *pstCl)
{
   char *pcFilename = NULL;

   //
   //   "GET /index.html HTTP/1.1"
   //   "Host: 10.0.0.231
   //   "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)
   //   "Accept: image/png,image/*;q=0.8,*/*;q=0.5
   //   "
   //   "Accept-Encoding: gzip,deflate
   //   "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
   //   "Keep-Alive: 115
   //   "Connection: keep-alive
   //   CR,CR,LF,CR,CR,LF
   //
   //    pcPathname
   //       iFileIdx : index to filename
   //       iExtnIdx : index to extension
   //
   PRINTF2("http_ExtractPageName(%d)=%s" CRLF, pstCl->iFileIdx, pstCl->pcPathname);
   if(pstCl->iFileIdx != -1)
   {
      pcFilename = &pstCl->pcPathname[pstCl->iFileIdx];
   }
   return(pcFilename);
}

//
// Function    : http_SendTextFile
// Description : Send a textfile from fs to socket
// 
// Parameters  : Client, filename
// Returns     : TRUE if OKee
//
static bool http_SendTextFile(NETCL *pstCl, char *pcFile)
{
   bool     fCc=FALSE;
   int      iRead, iTotal=0;
   FILE    *ptFile;
   char    *pcBuffer;
   char    *pcRead;

   ptFile = fopen(pcFile, "r");

   if(ptFile)
   {
      pcBuffer = malloc(READ_BUFFER_SIZE);
      //
      PRINTF1("http_SendTextFile(): File=<%s>"CRLF, pcFile);
      do
      {
         pcRead = fgets(pcBuffer, READ_BUFFER_SIZE, ptFile);
         if(pcRead)
         {
            iRead = strlen(pcBuffer);
            //PRINTF2("http_SendTextFile(): %s(%d)"CRLF, pcBuffer, iRead);
            HTTP_SendData(pstCl, pcBuffer, iRead);
            iTotal += iRead;
         }
         else
         {
            //PRINTF("http_SendTextFile(): EOF" CRLF);
         }
      }
      while(pcRead);
      //PRINTF1("http_SendTextFile(): %d bytes written."CRLF, iTotal);
      fclose(ptFile);
      free(pcBuffer);
      fCc = TRUE;
   }
   else
   {
      PRINTF1("http_SendTextFile(): Error open file <%s>"CRLF, pcFile);
   }
   return(fCc);
}

/*
 * Function    : http_Xmt 
 * Description : Send char to transmit buffer
 *
 * Parameters  : Client data, char
 * Returns     : TRUE if OKee
 *
 */
static bool http_Xmt(NETCL *pstCl, char cChar)
{
   bool  fCc=TRUE;

#ifdef   FEATURE_ECHO_HTTP_OUT
   //
   // Echo all HTTP request data from the client
   //
   ES_printf("%c", cChar);
#endif   //FEATURE_ECHO_HTTP_OUT
   //
   // Write the char into the XMT buffer. If the buffer is above the threshold put it out to the socket.
   // NET_WriteBuffer() does not return until all chars have been xmitted or an error has occurred.
   //
   pstCl->pcXmtBuffer[pstCl->iXmtBufferPut++] = cChar;
   if(pstCl->iXmtBufferPut >= WRITE_BUFFER_MAX)
   {
      if(NET_FlushCache(pstCl) < 0) fCc = FALSE;
   }
   return(fCc);
}


