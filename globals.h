/*  (c) Copyright:  2015  Patrn, Confidential Data
**
**  $Workfile:   globals.h
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            Define the mmap variables, common over all threads. If
**                      the structure needs to change, roll the revision number to make sure a new mmap file is created !
**                      
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       28 Oct 2015
**
 *  Revisions:
 *    $Log:   $
 *
 **/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <semaphore.h>
#include <sys/types.h>
#include <sys/mman.h>

#include "echotype.h"
#include "config.h"
#include "netfiles.h"

#define  MAX_PATH_LEN         500
//
#define  MAX_CMD_LEN          200
#define  MAX_ARG_LEN          1000
#define  MAX_CMDLINE_LEN      (MAX_CMD_LEN + MAX_ARG_LEN)
//
#define  MAX_TEMP_LEN         80
#define  MAX_PARM_LEN         16
#define  MAX_NUM_ALARMS       16
//
// Virtual LCD Size : 25 x 80 chars
//
#define  LCD_VIRTUAL_ROWS     25
#define  LCD_VIRTUAL_COLS     80
#define  LCD_VIRTUAL_LOG      14
#define  LCD_VIRTUAL_LOG_LINES 5
//
typedef enum KODISTAT
{
   KODI_STATUS_IDLE = 0,             //  is idle
   KODI_STATUS_ERROR,                //  is error
   KODI_STATUS_STOPPED,              //  is stopping
   KODI_STATUS_KEYPRESS,             //  has key pressed
   KODI_STATUS_KEYRELEASE,           //  has key released
   //
   NUM_KODI_STATUS
}  KODISTAT;


#ifdef   FEATURE_SHOW_COMMANDS
#define  GLOBAL_DUMPVARS(x)            GLOBAL_DumpVars(x)
#else    //FEATURE_SHOW_COMMANDS
#define  GLOBAL_DUMPVARS(x)
#endif   //FEATURE_SHOW_COMMANDS

typedef struct GLOBALS
{
   const char *pcDefault;
   int         iChanged;
   int         iChangedOffset;
   int         iValueOffset;
   int         iGlobalSize;
}  GLOBALS;

typedef struct VLCD
{
   char     pcVirtLcd[LCD_VIRTUAL_ROWS*LCD_VIRTUAL_COLS];
   u_int8   ubVirtRow, ubVirtCol;
   //
   bool     fLcdCursor;
   int      iLcdMode;
   u_int8   ubCursRow, ubCursCol;
}  VLCD;

typedef enum MAPSTATE
{
  MAP_CLEAR = 0,           // Clear all the mapped memory
  MAP_RESTART,             // Restart the counters using the last stored data

  NUM_MAPSTATES
} MAPSTATE;


typedef struct RPIMAP
{
   int         G_iSignature;
   u_int32     G_ulStartTimestamp;
   char        G_iVersionMajor;
   char        G_iVersionMinor;
   //
   // Misc semaphores
   //
   sem_t       G_tSemLcd;
   //
   VLCD        G_stVirtLcd;
   pid_t       G_tHostPid;
   pid_t       G_tSrvrPid;
   pid_t       G_tLircPid;
   pid_t       G_tVlcdPid;
   bool        G_HttpGetRequest;
   bool        G_HttpPostRequest;
   u_int32     G_ulSecondsCounter;
   int         G_iSpare_01;
   int         G_iFrontPanelKey;
   //
   char        G_pcMyIpIfce         [MAX_PARM_LEN];
   char        G_pcMyIpAddr         [INET_ADDRSTRLEN];
   char        G_pcIpAddr           [INET_ADDRSTRLEN];
   char        G_pcStatus           [MAX_PARM_LEN];
   char        G_pcVersionSw        [MAX_PARM_LEN];
   //
   // Change markers
   //
   u_int8      G_ubIpAddrChanged;
}  RPIMAP;

//
// Global functions
//
bool     GLOBAL_Close            (void);
void     GLOBAL_DumpVars         (char *);
void     GLOBAL_FillVirtualScreen(char);
RPIMAP  *GLOBAL_GetMapping       (void);
RPIMAP  *GLOBAL_Init             (void);
u_int32  GLOBAL_ReadSecs         (void);
void     GLOBAL_RestoreDefaults  (void);
bool     GLOBAL_Share            (void);
void     GLOBAL_Status           (int);
bool     GLOBAL_Sync             (void);
void     GLOBAL_UpdateSecs       (void);

#endif /* _GLOBALS_H_ */
