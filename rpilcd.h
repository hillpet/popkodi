/*  (c) Copyright:  2013  Patrn.nl, Confidential Data
**
**  $Workfile:          rpilcd.h
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            rpilcd header file
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       14 Oct 2015
**
 *  Revisions:
 *    $Log:   $
 *
 *
**/

#ifndef _RPILCD_H_
#define _RPILCD_H_

#include <wiringPi.h>
#include <wiringPiI2C.h>

#define  LCD_DISPLAY_ROWS     2     // Visible  : 2 x 16 chars
#define  LCD_DISPLAY_COLS     16    //
//
#define MODE_DP_OFF           0x0000
#define MODE_DP_ON            0x0001
#define MODE_BL_ON            0x0002
#define MODE_CLEAR            0x0004
#define MODE_LCD_ENABLED      0x8000
//
#define MODE_MASK             (MODE_BL_ON|MODE_DP_ON|MODE_CLEAR)
//
typedef enum LMODE
{
   LCD_MODE_DISPLAY_OFF    = 0,
   LCD_MODE_DISPLAY_ON,
   LCD_MODE_BACKLIGHT_OFF,
   LCD_MODE_BACKLIGHT_ON,
}  LMODE;

//
// Global prototypes
//
bool  LCD_ClearScreen      ();
bool  LCD_Close            ();
bool  LCD_FillScreen       (u_int8);
bool  LCD_Mode             (LMODE);
bool  LCD_Open             ();
bool  LCD_Read             (u_int8, u_int8, u_int8 *, int);
bool  LCD_Write            (u_int8, u_int8, u_int8 *, int);

#endif  /*_RPILCD_H_ */

