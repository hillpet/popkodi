/*  (c) Copyright:  2015  Patrn, Confidential Data  
 *
 *  $Workfile:          globals.c
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            Global variables for Raspberry pi KODI monitor
 *
 *
 *  Entry Points:       
 *
 *
 *  Compiler/Assembler: gnu gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       28 Oct 2015
 *
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
//
#include "globals.h"
#include "log.h"
#include "rtc.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local functions
//
static void    global_CreateMap        (void);
static void    global_InitMemory       (MAPSTATE);
static void    global_Open             (char *, int);
static void    global_SaveMapfile      ();
static void    global_RestoreMapfile   ();
//
// Static variables
//
static RPIMAP *pstRpiMap;
static int     iGlobalMapSize;
static int     iFdMap;
//
// These global settings are being initialized on every restart of the Apps.
//
const GLOBALS stGlobalParameters[] =
{
//    pcDefault         iChanged iChangedOffset                               iValueOffset                           iGlobalSize
   // --- Normal global parms ---------------------------------------------------------------------------------------------------------
   //
   {  "",               0,       -1,                                          offsetof(RPIMAP, G_pcMyIpAddr),        INET_ADDRSTRLEN  },
   {  "",               0,       offsetof(RPIMAP, G_ubIpAddrChanged),         offsetof(RPIMAP, G_pcIpAddr),          INET_ADDRSTRLEN  },
   // --- Misc ------------------------------------------------------------------------------------------------------------------------
   {  "Cam-Idle",       0,       -1,                                          offsetof(RPIMAP, G_pcStatus),          MAX_PARM_LEN     },
   {  VERSION,          0,       -1,                                          offsetof(RPIMAP, G_pcVersionSw),       MAX_PARM_LEN     },
   // --- Make sure all JSN_INT vars are "0" ------------------------------------------------------------------------------------------
   {  NULL,             0,       -1,                                          -1,                                    0                }
};

//
// These global settings are being initialized on request
//
const GLOBALS stRestoreParameters[] =
{
//    pcDefault         iChanged iChangedOffset                               iValueOffset                           iGlobalSize
   // --- Normal global parms ---------------------------------------------------------------------------------------------------------
   //
   {  "",               0,       offsetof(RPIMAP, G_ubIpAddrChanged),         offsetof(RPIMAP, G_pcIpAddr),          INET_ADDRSTRLEN  },
   // --- Misc ------------------------------------------------------------------------------------------------------------------------
   {  "Kodi-Idle",      0,       -1,                                          offsetof(RPIMAP, G_pcStatus),          MAX_PARM_LEN     },
   {  VERSION,          0,       -1,                                          offsetof(RPIMAP, G_pcVersionSw),       MAX_PARM_LEN     },
   // --- Make sure all JSN_INT vars are "0" ------------------------------------------------------------------------------------------
   {  NULL,             0,       -1,                                          -1,                                    0                }
};

const char *pcKodiStatus[] = 
{
   "Kodi-Idle",                         //KODI_STATUS_IDLE
   "Kodi-Error",                        //KODI_STATUS_ERROR
   "Kodi-Stopped",                      //KODI_STATUS_STOPPED
};

/*------  Local functions separator -----------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/**
 **  Name:         GLOBAL_Close
 **
 **  Description:  Close the main file with the mapping
 **
 **  Arguments:    
 **
 **  Returns:      TRUE if OKee
 **/
bool  GLOBAL_Close(void)
{
   munmap(pstRpiMap, iGlobalMapSize); 
   safeclose(iFdMap);
   global_SaveMapfile();
   return(TRUE);
}

/*
 * Function    : GLOBAL_FillVirtualScreen
 * Description : Fill the whole virtual screen
 *
 * Parameters  : Fill char
 * Returns     : 
 *
 */
void GLOBAL_FillVirtualScreen(char cChar)
{
   int      iRow;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   VLCD    *pstLcd=&pstMap->G_stVirtLcd;
   char    *pcVirtual=(char *)pstLcd->pcVirtLcd;

   for(iRow=0; iRow<LCD_VIRTUAL_ROWS; iRow++)
   {
      ES_memset(pcVirtual, cChar, LCD_VIRTUAL_COLS);
      pcVirtual += LCD_VIRTUAL_COLS;
   }
}

/*
 * Function    : GLOBAL_Init
 * Description : Init all global variables
 *
 * Parameters  : 
 * Returns     : MMAP ptr
 *
 */
RPIMAP *GLOBAL_Init(void)
{
   const GLOBALS *pstGlobals = stGlobalParameters;
   char          *pcValue;
   u_int8        *pubChanged;

   //
   // (Try to) copy the mmap file from SD to RAM disk
   //
   global_RestoreMapfile();
   //
   // Create the global MMAP mapping
   //
   global_CreateMap();
   //
   // Save HOST/SRVR pids
   //
   PRINTF1("GLOBAL_Init: RpiMap=%p" CRLF, pstRpiMap);
   //
   // Save Host   HOST pid
   // Save Server SRVR pid
   //
   pstRpiMap->G_tHostPid         = getpid();
   pstRpiMap->G_tSrvrPid         = getpid();
   pstRpiMap->G_tLircPid         = 0;
   pstRpiMap->G_tVlcdPid         = 0;
   pstRpiMap->G_iFrontPanelKey   = RCU_NOKEY;
   //
   GLOBAL_FillVirtualScreen(' ');
   //
   // Reset activity counter (recording/streaming)
   //
   pstRpiMap->G_ulSecondsCounter = 0;
   //
   // Load global settings every restart :
   //
   while(pstGlobals->pcDefault)
   {
      pcValue = (char *)pstRpiMap + pstGlobals->iValueOffset;
      strncpy(pcValue, pstGlobals->pcDefault, pstGlobals->iGlobalSize);
      if(pstGlobals->iChangedOffset >= 0)
      {
         pubChanged  = (u_int8 *)pstRpiMap + pstGlobals->iChangedOffset;
         *pubChanged = pstGlobals->iChanged;
         //PRINTF3("GLOBAL_Init: RpiMap[%04d] Chg=%d Value = <%s>" CRLF, pstGlobals->iValueOffset, *pubChanged, pcValue);
      }
      else
      {
         //PRINTF2("GLOBAL_Init: RpiMap[%04d] Chg=? Value = <%s>" CRLF, pstGlobals->iValueOffset, pcValue);
      }
      pstGlobals++;
   }
#ifdef FEATURE_RESTORE_DEFAULTS_ON_REBOOT
   //
   // Restore some defaults always
   //
   GLOBAL_RestoreDefaults();
#endif   //FEATURE_RESTORE_DEFAULTS_ON_REBOOT
   PRINTF1("GLOBAL_Init: Map=%p" CRLF, pstRpiMap);
   return(pstRpiMap);
}

/*
 * Function    : GLOBAL_RestoreDefaults
 * Description : Restore global variables
 *
 * Parameters  : 
 * Returns     : 
 *
 */
void GLOBAL_RestoreDefaults(void)
{
   const GLOBALS *pstGlobals = stRestoreParameters;
   char          *pcValue;
   u_int8        *pubChanged;

   //
   // Restore global settings
   //
   while(pstGlobals->pcDefault)
   {
      pcValue = (char *)pstRpiMap + pstGlobals->iValueOffset;
      strncpy(pcValue, pstGlobals->pcDefault, pstGlobals->iGlobalSize);
      if(pstGlobals->iChangedOffset >= 0)
      {
         pubChanged  = (u_int8 *)pstRpiMap + pstGlobals->iChangedOffset;
         *pubChanged = pstGlobals->iChanged;
         //PRINTF3("GLOBAL_RestoreDefaults: RpiMap[%04d] Chg=%d Value = <%s>" CRLF, pstGlobals->iValueOffset, *pubChanged, pcValue);
      }
      else
      {
         //PRINTF2("GLOBAL_RestoreDefaults: RpiMap[%04d] Chg=? Value = <%s>" CRLF, pstGlobals->iValueOffset, pcValue);
      }
      pstGlobals++;
   }
}

/*
 * Function    : GLOBAL_UpdateSecs
 * Description : Count seconds
 *
 * Parameters  : 
 * Returns     : 
 *
 */
void GLOBAL_UpdateSecs(void)
{
   pstRpiMap->G_ulSecondsCounter++;
   PRINTF1("GLOBAL_UpdateSecs: %x" CRLF, (unsigned int)pstRpiMap->G_ulSecondsCounter);
}

/*
 * Function    : GLOBAL_ReadSecs
 * Description : Return seconds counter
 *
 * Parameters  : 
 * Returns     : Running seconds counter
 *
 */
u_int32 GLOBAL_ReadSecs(void)
{
   PRINTF1("GLOBAL_ReadSecs: %x" CRLF, (unsigned int)pstRpiMap->G_ulSecondsCounter);
   return(pstRpiMap->G_ulSecondsCounter);
}

/*
 * Function    : GLOBAL_Status
 * Description : Update global status
 *
 * Parameters  : Cam-Status
 * Returns     : 
 *                pcKodiStatus
 */
void GLOBAL_Status(int iStatus)
{
   if(iStatus < NUM_KODI_STATUS)
   {
      strncpy(pstRpiMap->G_pcStatus, pcKodiStatus[iStatus], MAX_PARM_LEN);
      pstRpiMap->G_pcStatus[MAX_PARM_LEN-1] = 0;
      PRINTF1("GLOBAL_Status: %s" CRLF, pstRpiMap->G_pcStatus);
   }
}

/**
 **  Name:         GLOBAL_GetMapping
 **
 **  Description:  Get the current mapping
 **
 **  Arguments:    
 **
 **  Returns:      Ptr to current mapping
 **/
RPIMAP *GLOBAL_GetMapping(void)
{
  return(pstRpiMap);
}

/**
 **  Name:         GLOBAL_Sync
 **
 **  Description:  Sync the main file with the mapping
 **
 **  Arguments:    
 **
 **  Returns:      TRUE if OKee
 **/
bool  GLOBAL_Sync(void)
{
  msync(pstRpiMap, iGlobalMapSize, MS_SYNC);
  return(TRUE);
}

//
//  Function:   GLOBAL_Share
//  Purpose:    Start sharing the mmap file
//
//  Parms:      
//  Returns:    True if OKee
//
bool GLOBAL_Share(void)
{
   bool     fCc=TRUE;

   pstRpiMap = (RPIMAP *) mmap(NULL, iGlobalMapSize, PROT_READ|PROT_WRITE, MAP_SHARED, iFdMap, 0);
   if(pstRpiMap == MAP_FAILED)
   {
      LOG_Report(0, "MAP", "Re-mapping failed");
      PRINTF("GLOBAL_Share(): remapping failed" CRLF);
      fCc = FALSE;
   }
   else
   {
      PRINTF1("GLOBAL_Share: Map=%p" CRLF, pstRpiMap);
   }
   return(fCc);
}

#ifdef   FEATURE_SHOW_COMMANDS
//
//  Function:  GLOBAL_DumpVars
//  Purpose:   Dump the MMAP variables
//  Parms:     Owner
//
//  Returns:
//
void GLOBAL_DumpVars(char *pcSrc)
{
   ES_printf("%s(): Mapfile    : [    ] %p"     CRLF, pcSrc, pstRpiMap);
   ES_printf("%s(): Version    : [%4d] v%d.%d"  CRLF, pcSrc, offsetof(RPIMAP, G_iVersionMajor), pstRpiMap->G_iVersionMajor, pstRpiMap->G_iVersionMinor);
   ES_printf("%s(): Signature  : [%4d] 0x%x"    CRLF, pcSrc, offsetof(RPIMAP, G_iSignature),    pstRpiMap->G_iSignature);
   //
   ES_printf("%s(): Host       : [%4d] %d"      CRLF, pcSrc, offsetof(RPIMAP, G_tHostPid),      pstRpiMap->G_tHostPid);
   ES_printf("%s(): Server     : [%4d] %d"      CRLF, pcSrc, offsetof(RPIMAP, G_tSrvrPid),      pstRpiMap->G_tSrvrPid);
   //
   ES_printf("%s(): IP         : [%4d] <%s>"    CRLF, pcSrc, offsetof(RPIMAP, G_pcMyIpAddr),    pstRpiMap->G_pcMyIpAddr);
   ES_printf("%s(): IP         : [%4d] <%s>"    CRLF, pcSrc, offsetof(RPIMAP, G_pcIpAddr),      pstRpiMap->G_pcIpAddr);
   ES_printf(CRLF);
   //sleep(1);
}
#endif   //FEATURE_SHOW_COMMANDS

/*------  Local functions separator -----------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
//  Function:   global_CreateMap
//  Purpose:    Read the mapping from the file
//  Parms:
//
//  Returns:
//
static void global_CreateMap(void)
{
   //
   // Open the mmap file
   //
   global_Open(RPI_MAP_FILE, sizeof(RPIMAP));
   if(pstRpiMap == NULL ) 
   {
      ES_printf("global_CreateMap(): MMAP open error");
      exit(254);
   }
   //
   if((pstRpiMap->G_iSignature != DATA_VALID_SIGNATURE)
        ||
      (pstRpiMap->G_iVersionMajor != DATA_VERSION_MAJOR))
   {
      // Bad mapping space !
      ES_printf("global_CreateMap():Bad map: Initialize...." CRLF);
      ES_printf("global_CreateMap():Old Signature  = 0x%08x" CRLF, pstRpiMap->G_iSignature);
      ES_printf("global_CreateMap():Old DB version = v%d.%d" CRLF, pstRpiMap->G_iVersionMajor, pstRpiMap->G_iVersionMinor);
      //
      LOG_Report(0, "MAP", "Map has changed :");
      LOG_Report(0, "MAP", "Sig = %08x", pstRpiMap->G_iSignature);
      LOG_Report(0, "MAP", "Vrs = v%d.%d", pstRpiMap->G_iVersionMajor, pstRpiMap->G_iVersionMinor);
      //
      global_InitMemory(MAP_CLEAR);
      GLOBAL_RestoreDefaults();
      //
      LOG_Report(0, "MAP", "New Mapping :");
      LOG_Report(0, "MAP", "Sig = %08x", pstRpiMap->G_iSignature);
      LOG_Report(0, "MAP", "Vrs = v%d.%d", pstRpiMap->G_iVersionMajor, pstRpiMap->G_iVersionMinor);
      //
      ES_printf("global_CreateMap():Mapping changed: Initialize...." CRLF);
      ES_printf("global_CreateMap():New signature  = 0x%08x" CRLF, pstRpiMap->G_iSignature);
      ES_printf("global_CreateMap():New DB version = v%d.%d" CRLF, pstRpiMap->G_iVersionMajor, pstRpiMap->G_iVersionMinor);
   }
   else
   {
      ES_printf("global_CreateMap():Mapping OKee, v%d.%d" CRLF, pstRpiMap->G_iVersionMajor, pstRpiMap->G_iVersionMinor);
      global_InitMemory(MAP_RESTART);
   }
}

//
//  Function:   global_InitMemory
//  Purpose:    Init the MAP memory
//
//  Parms:      Cmd
//  Returns:
//
static void global_InitMemory(MAPSTATE tState)
{
  int      iIdx;
  u_int32  ulTime;

  switch(tState)
  {
     case MAP_CLEAR:
        ES_memset(pstRpiMap, 0, sizeof(RPIMAP));
        pstRpiMap->G_iSignature       = DATA_VALID_SIGNATURE;
        pstRpiMap->G_iVersionMajor    = DATA_VERSION_MAJOR;
        pstRpiMap->G_iVersionMinor    = DATA_VERSION_MINOR;
        pstRpiMap->G_ulStartTimestamp = RTC_GetDateTime(NULL);
        break;

     case MAP_RESTART:
        ulTime = RTC_GetDateTime(NULL);
        if(ulTime > (pstRpiMap->G_ulStartTimestamp+ES_SECONDS_PER_DAY))
        {
           // it's more than a day agoo:
           iIdx = 0;
           LOG_Report(0, "MAP", "InitMemory:Restart new day");
        }
        else
        {
           // Get the current quarter 0...(24*4)-1
           iIdx = RTC_GetCurrentQuarter(0);
           PRINTF1("global_InitMemory():Restart %d quarter" CRLF, iIdx);
           LOG_Report(0, "MAP", "InitMemory:Restart %d quarter", iIdx);
        }
        break;

     default:
        break;
  }
}

//
//  Function:   global_Open
//  Purpose:    Open the main file for the mapping
//
//  Parms:      Filename, size
//  Returns:    
//
static void global_Open(char *pcName, int iMapSize)
{
  pstRpiMap      = NULL;
  iGlobalMapSize = 0;

  iFdMap = safeopen2(pcName, O_RDWR|O_CREAT, 0640);
  //
  // Make sure the file has the minimum size!
  //
  lseek(iFdMap, iMapSize, SEEK_SET);
  safewrite(iFdMap, "EOF", 4);
  //
  pstRpiMap = (RPIMAP *) mmap(NULL, iMapSize, PROT_READ|PROT_WRITE, MAP_SHARED, iFdMap, 0);
  if(pstRpiMap == MAP_FAILED)
  {
     LOG_Report(0, "MAP", "Mapping failed");
     ES_printf("global_Open():Mapping failed" CRLF);
  }
  else
  {
     iGlobalMapSize = iMapSize;
  }
}

//
//  Function:  global_SaveMapfile
//  Purpose:   Copy the MMAP file from RAM disk to SD 
//             cp /usr/local/share/rpi/popkodi.map /mnt/rpicache/popcodi.map
//  Parms:     
//  Returns:    
//
static void global_SaveMapfile()
{
   int   iCc; 
	char *pcShell="cp " RPI_WORK_DIR "popkodi.* " RPI_BACKUP_DIR;

   PRINTF1("global_SaveMapfile(): backup log, map files [%s]" CRLF, pcShell);
   iCc = system(pcShell);
   if(iCc) 
   {
      PRINTF2("global_RestoreMapfile():Error-%d: Backup log, map files [%s]" CRLF, errno, pcShell);
      LOG_Report(0, "GLO", "Error: Backup log, map files [%s]", pcShell);
   }
}

//
//  Function:  global_RestoreMapfile
//  Purpose:   Copy the MMAP file from SD to RAM disk
//             cp /mnt/rpicache/popkodi.map /usr/local/share/rpi/popkodi.map
//  Parms:     
//  Returns:    
//
static void global_RestoreMapfile()
{
   int   iCc; 
	char *pcShell="cp " RPI_BACKUP_DIR "popkodi.* " RPI_WORK_DIR;

   PRINTF1("global_RestoreMapfile(): restore log, map files [%s]" CRLF, pcShell);
   iCc = system(pcShell);
   if(iCc) 
   {
      PRINTF2("global_RestoreMapfile():Error-%d: Restore log, map files [%s]" CRLF, errno, pcShell);
      LOG_Report(errno, "GLO", "Error: Restore log, map files [%s]", pcShell);
   }
}

