/*  (c) Copyright:  2013  Patrn.nl, Confidential Data
**
**  $Workfile:          emulip.h
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            emulip header file
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       5 Dec 2013
**
 *  Revisions:
 *    $Log:   $
 *
 *
**/

#ifndef _EMULIP_H_
#define _EMULIP_H_

//
// Global prototypes
//
pid_t IP_SendToServer         (char *);

#endif  /*_EMULIP_H_ */

