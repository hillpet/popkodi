/*  (c) Copyright:  2013  Patrn.nl, Confidential Data
**
**  $Workfile:          rpicad
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            Interface PifaceCad functions
**
**  Entry Points:       
**
**
**
**
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       25 Dec 2013
**
 *  Revisions:
 *    $Log:   $
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>

#include "config.h"
#include "echotype.h"
#include "netfiles.h"
#include "log.h"
#include "globals.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static int ip_SendRequest           (char *);

//
//  Function:   IP_SendToServer
//  Purpose:    Send HTTP request string via IP to local server
//
//  Parms:      Data
//  Returns:    Pid if OKee send, else -1 on error
//
pid_t IP_SendToServer(char *pcData)
{
   pid_t tPid;
   int   iCc;

   //==========================================================================
   // Split process : 
   //       EXEC:    returns with tPid of the IPCOM
   //       IPCOM:   returns with tPid=0
   //
   tPid = fork();
   //
   //==========================================================================
   switch(tPid)
   {
      case 0:
         // IPCOM emulate thread: send out IP data to local host
         PRINTF1("IP_SendToServer(): Emul=%d" CRLF, tPid);
         //LOG_Report(0, "IPC", "IP_SendToServer(): Emul=%d", tPid);
         iCc = ip_SendRequest(pcData);
         _exit(iCc);
   
      case -1:
         // Error
         PRINTF("IP_SendToServer(): IP_SendToServer(): Helper ERROR" CRLF);
         LOG_Report(errno, "IPC", "IP_SendToServer(): Helper ERROR");
         break;

      default:
         // EXEC returns here: 
         PRINTF1("IP_SendToServer(): Caller returns, Emul=%d" CRLF, tPid);
         break;
   }
   return(tPid);
}


/*------  Local functions separator ------------------------------------
__Local_functions_(){};
----------------------------------------------------------------------------*/

/*
 *  Function:     ip_SendRequest
 *  Description:  Send data to local IP stack
 *
 *  Arguments:    Data to send, length
 *  Returns:      CC (0=OKee)
 *
 */
static int ip_SendRequest(char *pcData)
{
   int      iCc=0;
   int      iSend;
   int      iRead, iTotal=0;
   int      iFd;
   char    *pcIp="127.0.0.1";
   char    *pcPort="8080";
   char     pcBuffer[84];

   iFd = NET_ClientConnect(pcIp, pcPort, "tcp");
   //
   if(iFd > 0)
   {
      PRINTF("ip_SendRequest(): Connect to server OK !" CRLF);
      iSend = NET_WriteString(iFd, (char *) pcData);
      if(iSend > 0)
      {
         PRINTF1("ip_SendRequest():%d bytes send" CRLF, iSend);
         //pwjh sleep(3);
         do
         {
            iRead = read(iFd, pcBuffer, 80);
            if(iRead < 0)
            {
               PRINTF1("ip_SendRequest():ERROR %d reading socket" CRLF, iRead);
            }
            else
            {
               //PRINTF1("ip_SendRequest():%s" CRLF, pcBuffer);
               iTotal += iRead;
            }
         }
         while(iRead > 0);
         //
         PRINTF1("ip_SendRequest():%d bytes read" CRLF, iTotal);
      }
      else
      {
         iCc=errno;
         PRINTF1("ip_SendRequest():ERROR: %d bytes send" CRLF, iSend);
      }
      NET_ClientDisconnect(iFd);
   }
   else
   {
      iCc=errno;
      PRINTF1("ip_SendRequest(): Not able to connect to server %s !" CRLF, pcIp);
   }
   return(iCc);
}   

