/*  (c) Copyright:  2012  Patrn, Confidential Data
 *
 *  $Workfile:          httppage.h
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            Header file for HTTP web server helper files
 *
 *
 *  Entry Points:       
 *
 *
 *
 *  Compiler/Assembler: 
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
 *
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _HTTPPAGE_H_
#define _HTTPPAGE_H_

#include "netfiles.h"

#define  NEWLINE        "\r\n"

typedef enum
{
   HTTP_CHAR = 0,
   HTTP_GET,
   HTTP_POST,
   HTTP_ERROR,
   HTTP_END
}  RTYPE;

typedef enum
{
   HTTP_ATTR_HTML    =  0x0001,
   HTTP_ATTR_TABLE   =  0x0002,
   HTTP_ATTR_TBODY   =  0x0004,
   HTTP_ATTR_BODY    =  0x0008,
   HTTP_ATTR_TR      =  0x0010,
   HTTP_ATTR_TD      =  0x0011
}  HTTP_ATTR;

typedef enum
{
   HTTP_GEN = 0,
   HTTP_SHTML,
   HTTP_HTML,
   HTTP_JS,
   HTTP_CGI,
   HTTP_GIF,
   HTTP_JPG,
   HTTP_BMP,
   HTTP_PNG,
   HTTP_TXT,
   HTTP_JSON,
   HTTP_ZIP,
   HTTP_CSS,
   HTTP_MP3,
   HTTP_MP4,
   HTTP_H264,
   HTTP_ICO
}  FTYPE;

typedef bool (*PFEXT)(NETCL *);
typedef struct HTTPTYPE
{
   char    *pcExt;
   FTYPE    tType;
   char    *pcType;
   PFEXT    pfExt;   
}  HTTPTYPE;

typedef RTYPE (*PFHTTP)(NETCL *, char *, int);

typedef struct HTTPREQ
{
   char    *pcHeader;
   PFHTTP   pfHandler;
}  HTTPREQ;


//
// Global prototypes
//
bool     HTTP_RequestStaticPage        (NETCL *);
RTYPE    HTTP_CollectRequest           (NETCL *);
bool     HTTP_CollectGet               (NETCL *, char);
char    *HTTP_CollectGetParameter      (NETCL *);
char    *HTTP_CollectGetParameterValue (NETCL *);

#endif   //_HTTPPAGE_H_
