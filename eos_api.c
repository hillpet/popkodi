/*  (c) Copyright:  2012 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:           Offer the API as an abstraction layer for EOS calls
**
**
**  Entry Points:
**                     ES_printf()
**                     ES_timeprintf()
**                     REPORT_String()
**
**
**
**
**
**
 *  Compiler/Assembler: Linux 2.6
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 * v100:Initial revision.
 *
 **/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <malloc.h>
#include <signal.h>
#include <errno.h>
#include <stdarg.h>
#include <time.h>

#include "echotype.h"
#include "rtc.h"
#include "eos_api.h"

//#define USE_PRINTF
#include "printf.h"


/* ======   Local Functions separator ===========================================
void ___main_functions(){}
==============================================================================*/

/**
 **  Name:         ES_printf
 **
 **  Description:  Printf abstraction
 **
 **  Arguments:
 **
 **  Returns:
 **/
int ES_printf(char *pcFormat, ...)
{
   va_list  tArg;
   int      iRet;

   va_start(tArg, pcFormat );
   iRet = vprintf(pcFormat, tArg);
   va_end(tArg);
   return(iRet);
}

/**
 **  Name:         ES_timeprintf
 **
 **  Description:  Printf abstraction with time stamp
 **
 **  Arguments:
 **
 **  Returns:
 **/
int ES_timeprintf(char *pcFormat, ...)
{
  va_list      tArgs;
  struct tm   *pstTime;
  time_t       tSec;
  int          iRet;

  tSec = (time_t) RTC_GetSystemSecs();
  pstTime = localtime(&tSec);

  printf("%02d:%02d:%02d %5d| ",
       pstTime->tm_hour,
       pstTime->tm_min,
       pstTime->tm_sec,
       getpid());

  va_start(tArgs, pcFormat);
  iRet = vprintf(pcFormat, tArgs);
  return(iRet);
}


/**
 **  Name:         REPORT_String
 **
 **  Description:  Printf abstraction
 **
 **  Arguments:
 **
 **  Returns:
 **/
int REPORT_String(char *pcFormat, ...)
{
   va_list  tArg;
   int      iRet;

   va_start(tArg, pcFormat );
   iRet = vprintf(pcFormat, tArg );
   va_end( tArg );
   return(iRet);
}
