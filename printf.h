/*  (c) Copyright:  2012 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            Generic printer redirections
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
**
 *  Revisions:
 *    $Log:   $
 * v100:Initial revision.
 *
 *
**/

#ifndef  _PRINTF_H_
#define  _PRINTF_H_

#include "eos_api.h"

//
// Define CRLF string : PRINTF(CRLF"This is the text");
//
#define CRLF                  "\n"

#if (defined FEATURE_USE_PRINTF && defined USE_PRINTF)
   #define  PRINTF(a)                  printf(a)
   #define  PRINTF1(a,b)               printf(a,b)
   #define  PRINTF2(a,b,c)             printf(a,b,c)
   #define  PRINTF3(a,b,c,d)           printf(a,b,c,d)
   #define  PRINTF4(a,b,c,d,e)         printf(a,b,c,d,e)
   #define  PRINTF5(a,b,c,d,e,f)       printf(a,b,c,d,e,f)
   #define  PRINTF6(a,b,c,d,e,f,g)     printf(a,b,c,d,e,f,g)
   #define  PRINTF7(a,b,c,d,e,f,g,h)   printf(a,b,c,d,e,f,g,h)
#else
   #define  PRINTF(a)
   #define  PRINTF1(a,b)
   #define  PRINTF2(a,b,c)
   #define  PRINTF3(a,b,c,d)
   #define  PRINTF4(a,b,c,d,e)
   #define  PRINTF5(a,b,c,d,e,f)
   #define  PRINTF6(a,b,c,d,e,f,g)
   #define  PRINTF7(a,b,c,d,e,f,g,h)
#endif   // defined USE_PRINTF


#endif   // _PRINTF_H_

