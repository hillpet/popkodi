/*  (c) Copyright:  2014  Patrn.nl, Confidential Data 
**
**  $Workfile:          rpircu
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            Interface PifaceCad IR RCU functions
**
**  Entry Points:       
**
**
**
**
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       20 Feb 2014
**
 *  Revisions:
 *    $Log:   $
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include "config.h"
#include "echotype.h"
#include "netfiles.h"
#include "log.h"
#include "rtc.h"
#include "globals.h"
#include "rpilcd.h"
#include "fpkeys.h"

#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static pid_t   fp_CreateLcdHandler     (void);
static pid_t   fp_GetPidByName         (const char *);
static int     fp_KillProcess          (pid_t, bool);
static void    fp_SetDisplayCoords     (u_int8, u_int8, int);
static pid_t   fp_StartProcess         (const char *, const char *, const char *, int);
static void    fp_TerminateMedia       (void);
static void    fp_ToggleDisplay        (int);
static void    fp_WriteVirtualLcd      (u_int8, u_int8, const char *);
//
static timer_t lcd_EnableTimer         (int, int, int);
static void    lcd_HandleLcd           (void);
static void    lcd_ReceiveSignalInt    (int);
static void    lcd_ReceiveSignalUser1  (int);
static void    lcd_ReceiveSignalUser2  (int);
static void    lcd_ReceiveSignalTerm   (int);
static void    lcd_SetTimer            (int);
static bool    lcd_SignalRegister      (sigset_t *);
static void    lcd_TimerHandler        (int, siginfo_t *, void *);
//
//
//#define  FEATURE_SHOW_PIDS
#ifdef   FEATURE_SHOW_PIDS
 static void                 fp_ShowMediaPids();
 #define FP_SHOWMEDIAPIDS()  fp_ShowMediaPids()
#else
 #define FP_SHOWMEDIAPIDS()
#endif   //FEATURE_SHOW_PIDS
//
//
//#define  FEATURE_SHOW_VIRTUAL_SCREEN
#ifdef   FEATURE_SHOW_VIRTUAL_SCREEN
 static void fp_ShowVirtialScreen(void);
 #define FP_SHOWVIRTIALSCREEN()  fp_ShowVirtialScreen()
#else
 #define FP_SHOWVIRTIALSCREEN()
#endif   //FEATURE_SHOW_VIRTUAL_SCREEN
//
static FPSTATE fp_DispExec       (FPSTATE, u_int8);
static FPSTATE fp_DispExit       (FPSTATE, u_int8);
static FPSTATE fp_DispInit       (FPSTATE, u_int8);
static FPSTATE fp_DispTogL       (FPSTATE, u_int8);
static FPSTATE fp_DispTogR       (FPSTATE, u_int8);
static FPSTATE fp_KodiExit       (FPSTATE, u_int8);
static FPSTATE fp_KodiInit       (FPSTATE, u_int8);
static FPSTATE fp_NoAction       (FPSTATE, u_int8);

//
// Local variables
//
static bool       fTmrRunning    = TRUE;
static timer_t    tTimerId;
static int        iTimePerSec    = 0;
static int        iTimePerSecDef = TIME_PER_SECS;
//
static pid_t      tPidLcdd       = 0;
static pid_t      tPidLcdp       = 0;
static pid_t      tPidKodi       = 0;
static FPMODE     tFpMode        = FPM_OFF;
//
// LCD Strings
//                                 "0123456789012345"
static const char *pcPowerup1    = " Raspberry-Kodi ";
static const char *pcPowerup2    = "  Media Player  ";
static const char *pcDispKodi    = " Start-up  Kodi ";
static const char *pcExitKodi    = "   Close Kodi   ";
static const char *pcExecCmnd    = "    Command :   ";
static const char *pcCmndBoot    = "  Reboot System ";
static const char *pcCmndPoff    = "   Power Off    ";
static const char *pcCmndExit    = "  Exit PopKodi  ";
//
static const char *pcExecBoot    = "sudo reboot";
static const char *pcExecPoff    = "sudo poweroff";
static const char *pcParmNone    = "";

static const char *pcPathKodi    = "/usr/lib/kodi/kodi.bin";
static const char *pcCmndKodi    = "kodi";
static const char *pcParmKodi    = "";
//
static const char *pcPathLcdp    = "lcdproc";
static const char *pcCmndLcdp    = "lcdproc";
static const char *pcParmLcdp    = "";
//
static const char *pcPathLcdd    = "LCDd";
static const char *pcCmndLcdd    = "LCDd";
static const char *pcParmLcdd    = "";
//
static const FPKEYH stFpKeyHandler[NUM_FP_STATES][NUM_RCU_KEYS] =
{                                                                                                  
/*                 RCU_NOKEY                 RCU_POWER                 RCU_ENTER                 RCU_UP                    RCU_DOWN                  RCU_LEFT                  RCU_RIGHT                 RCU_PLAY                  RCU_STOP  */
/* FPS_NONE */ { { fp_DispInit, FPS_STBY}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE} },
/* FPS_STBY */ { { fp_NoAction, FPS_NONE}, { fp_KodiInit, FPS_KODI}, { fp_DispExec, FPS_STBY}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_DispTogL, FPS_STBY}, { fp_DispTogR, FPS_STBY}, { fp_DispTogL, FPS_STBY}, { fp_DispTogR, FPS_STBY} },
/* FPS_KODI */ { { fp_NoAction, FPS_NONE}, { fp_KodiExit, FPS_STBY}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE} },
/* FPS_DISP */ { { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE} },
/* FPS_EXIT */ { { fp_DispExit, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE}, { fp_NoAction, FPS_NONE} } 
};


/* ======   Local Functions separator ===========================================
void ___main_functions(){}
==============================================================================*/

//
//  Function:  FP_KeyExit
//  Purpose:   Exit FP Key and LCD handler
//
//  Parms:     
//  Returns:   
//
void FP_KeyExit(void)
{
   LCD_Close();
}

//
//  Function:  FP_KeyHandler
//  Purpose:   Handle a new Frontpanel key
//
//  Parms:     State, fp-key
//  Returns:   New state
//
FPSTATE FP_KeyHandler(FPSTATE tState, u_int8 ubKey)
{
   const FPKEYH *pstKeyHdlr=&stFpKeyHandler[tState][ubKey];

   FP_SHOWMEDIAPIDS();
   //
   PRINTF2("FP_KeyHandler():CurState=%d, Key=%d" CRLF, tState, ubKey);
   tState = pstKeyHdlr->pfHandler(tState, ubKey);
   PRINTF1("FP_KeyHandler():NewState=%d" CRLF, tState);
   //
   return(tState);
}

//
//  Function:  FP_KeyInit
//  Purpose:   Init FP Key and LCD handler
//
//  Parms:     
//  Returns:   
//
void FP_KeyInit(void)
{
   RPIMAP       *pstMap=GLOBAL_GetMapping();
   VLCD         *pstLcd=&pstMap->G_stVirtLcd;

   if( !LCD_Open() ) 
   {
      PRINTF1("fp_DispInit():Init error:%d" CRLF, errno);
      LOG_Report(errno, "FPK", "Init error");
   }
   pstLcd->iLcdMode = 0;
}


/* ======   Local Functions separator ===========================================
void ___Local_functions(){}
==============================================================================*/

// 
//  Function:  fp_CreateLcdHandler
//  Purpose:   Run a separate thread to handle the LCD module
// 
//  Parms:     
//  Returns:   The new pid (parent), 0 (child) or -1 error
// 
// 
static pid_t fp_CreateLcdHandler(void)
{
   pid_t    tPid;

   //==========================================================================
   // Split process : 
   //       Parent thread returns with tPid of the new thread
   //       Child  thread returns with tPid=0
   //
   tPid = fork();
   //
   //==========================================================================
   switch(tPid)
   {
      case 0:
         lcd_HandleLcd();
         //
         // the lcd thread runs until popkodi terminates.
         //
         _exit(254);
         break;

      case -1:
         PRINTF1("fp_CreateLcdHandler(): Error: %d" CRLF, errno);
         break;
            
      default:
         //
         // This is the parent with the thread of the child
         //
         PRINTF1("fp_CreateLcdHandler():Vlcd pid=%d" CRLF, tPid);
         break;
   }
   return(tPid);
}

//
//  Function:  fp_GetPidByName
//  Purpose:   Retrieve  a process ID by name
//
//  Parms:     Process name
//  Returns:   tPid, -1 on error, 0 on not-found PID
//
#define LINE_LEN   15
#define BUFF_LEN   63
//
static pid_t fp_GetPidByName(const char *pcName)
{
   pid_t    tPid=-1;
   char     cLine[LINE_LEN+1];
   char     cBuff[BUFF_LEN+1];
   FILE    *ptFp;

   ES_snprintf(cBuff, BUFF_LEN, "pidof %s", pcName);
   if(strlen(cBuff) >= BUFF_LEN)
   {
      PRINTF("fp_GetPidByName():ERROR:Pidlength exceeds buffer !!");
      LOG_Report(0, "POP", "fp_GetPidByName():ERROR:Pidlength exceeds buffersize !");
   }
   else
   {
      ptFp = popen(cBuff, "r");
      //
      fgets(cLine, LINE_LEN, ptFp);
      tPid = strtoul(cLine, NULL, 10);
      //
      pclose(ptFp);
   }
   return(tPid);
}

// 
//  Function:  fp_KillProcess
//  Purpose:   Kill an ongoing process (if any)
// 
//  Parms:     pid to kill, friendly YESNO
//  Returns:   completion code (0=ok)
// 
// 
static int fp_KillProcess(pid_t tPid, bool fFriendly)
{
   int      iCc=0;
   pid_t    tPidAct;
   int      iStatus;
   char     cShell[32];

   if(tPid)
   {
      //
      // Ongoing action: terminate friendly or forced
      //
      if(fFriendly)
      {
         kill(tPid, SIGTERM);
         //
         //LOG_Report(0, "FPK", "rpi_KillProcess(): Friendly ask background thread pid=%d to exit", tPid);
         PRINTF1("fp_KillProcess(): Friendly ask background thread pid=%d to exit" CRLF, tPid);
      }
      else
      {
         LOG_Report(0, "FPK", "rpi_KillProcess(): Terminate background thread (pid=%d)", tPid);
         PRINTF1("fp_KillProcess(): Kill background thread: pid=%d" CRLF, tPid);
         ES_sprintf(cShell, "kill -kill %d", tPid);
         iCc  = system(cShell);
         while( ((tPidAct = waitpid(tPid, &iStatus, 0)) == -1) && (errno == EINTR) )
           continue;       /* Restart if interrupted by handler */ 
         //
         PRINTF2("fp_KillProcess(): Kill background thread: pid=%d, Done (CC=%d)" CRLF, tPidAct, iCc);
      }
   }
   return(iCc);
}

//
//  Function:  fp_SetDisplayCoords
//  Purpose:   Set the row, col for the virtual LCD display
//
//  Parms:     Row, Col, mode
//  Returns:   
//
static void fp_SetDisplayCoords(u_int8 ubRow, u_int8 ubCol, int iMode)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   VLCD    *pstLcd=&pstMap->G_stVirtLcd;;

   pstLcd->ubVirtRow = ubRow;
   pstLcd->ubVirtCol = ubCol;
   //
   iMode &= MODE_MASK;
   pstLcd->iLcdMode &= ~MODE_MASK;
   pstLcd->iLcdMode |= iMode;
}

#ifdef   FEATURE_SHOW_PIDS
//
//  Function:  fp_ShowMediaPids
//  Purpose:   Display the pids of the media execs
//
//  Parms:     
//  Returns:   
//
static void fp_ShowMediaPids(void)
{
   pid_t tPid;

   tPid = fp_GetPidByName(pcPathLcdd);
   PRINTF2("fp_ShowMediaPids():%s=%d" CRLF, pcPathLcdd, tPid);
   //
   tPid = fp_GetPidByName(pcPathLcdp);
   PRINTF2("fp_ShowMediaPids():%s=%d" CRLF, pcPathLcdp, tPid);
   //
   tPid = fp_GetPidByName(pcPathKodi);
   PRINTF2("fp_ShowMediaPids():%s=%d" CRLF, pcPathKodi, tPid);
}
#endif   //FEATURE_SHOW_PIDS

#ifdef   FEATURE_SHOW_VIRTUAL_SCREEN
//
//  Function:  fp_ShowVirtialScreen
//  Purpose:   Display the virtual screen
//
//  Parms:     
//  Returns:   
//
static void fp_ShowVirtialScreen(void)
{
   int      iRow;
   char    *pcTemp;
   char    *pcVirtual;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   VLCD    *pstLcd=&pstMap->G_stVirtLcd;

   pcTemp = safemalloc(LCD_VIRTUAL_COLS+1);
   //
   pcVirtual = (char *)pstLcd->pcVirtLcd;
   for(iRow=0; iRow<LCD_VIRTUAL_ROWS; iRow++)
   {
      ES_memcpy(pcTemp, pcVirtual, LCD_VIRTUAL_COLS);
      pcTemp[LCD_VIRTUAL_COLS] = 0;
      ES_printf("fp_ShowVirtialScreen(%02d):[%s]" CRLF, iRow, pcTemp);
      pcVirtual += LCD_VIRTUAL_COLS;
   }
   //
   safefree(pcTemp);
}
#endif   //FEATURE_SHOW_VIRTUAL_SCREEN

// 
//  Function:  fp_StartProcess
//  Purpose:   Start a process
// 
//  Parms:     Process name, process parms, process pidpath, timeout (secs)
//  Returns:   Process pid
// 
// 
static pid_t fp_StartProcess(const char *pcProcess, const char *pcParms, const char *pcPidPath, int iTimeout)
{
   int      iCc, iSize;
   pid_t    tPid;
   char    *pcShell;

   //==========================================================================
   // Split process : 
   //       CMND thread returns with tPid of the EXEC thread
   //       EXEC thread returns with tPid=0
   //
   tPid = fork();
   //
   //==========================================================================
   switch(tPid)
   {
      case 0:
         //
         // This is the new process thread
         //
         iSize   = (int) strlen(pcProcess) + (int) strlen(pcParms) + 8;
         pcShell = safemalloc(iSize);
         //
         ES_sprintf(pcShell, "%s%s", pcProcess, pcParms);
         PRINTF1("fp_StartProcess(): Exec system(%s)" CRLF, pcShell);
         iCc = system(pcShell);
         PRINTF1("fp_StartProcess(): Exec completion: cc=%d" CRLF, iCc);
         safefree(pcShell);
         _exit(iCc);

      case -1:
         //
         // Error
         //
         PRINTF1("fp_StartProcess(): Error: %d" CRLF, errno);
         break;
            
      default:
         //
         // This is the parent with the thread of the child
         //
         PRINTF2("fp_StartProcess(): %s-startup-pid=%d" CRLF, pcPidPath, tPid);
         do
         {
            tPid = fp_GetPidByName(pcPidPath);
            PRINTF2("fp_StartProcess(): %s-pid=%d" CRLF, pcPidPath, tPid);
            sleep(1);
         }
         while((--iTimeout > 0) && (tPid == 0));
         break;
   }
   return(tPid);
}

//
//  Function:  fp_TerminateMedia
//  Purpose:   Make sure to terminate the media executables
//
//  Parms:     
//  Returns:   
//
static void fp_TerminateMedia(void)
{
   do
   {
      if( (tPidLcdp = fp_GetPidByName(pcPathLcdp)) > 0)   
      {
         PRINTF1("fp_TerminateMedia():%s" CRLF, pcPathLcdp);
         fp_KillProcess(tPidLcdp, TRUE);
         sleep(1);
      }
   }
   while(tPidLcdp);
   //
   do
   {
      if( (tPidLcdd = fp_GetPidByName(pcPathLcdd)) > 0)
      {
         PRINTF1("fp_TerminateMedia():%s" CRLF, pcPathLcdd);
         fp_KillProcess(tPidLcdd, TRUE);
         sleep(1);
      }
   }
   while(tPidLcdd);
   //
   do
   {
      if( (tPidKodi = fp_GetPidByName(pcPathKodi)) > 0)
      {
         PRINTF1("fp_TerminateMedia():%s" CRLF, pcPathKodi);
         fp_KillProcess(tPidKodi, TRUE);
         sleep(1);
      }
   }
   while(tPidKodi);
   PRINTF("fp_TerminateMedia():OK" CRLF);
}

//
//  Function:  fp_ToggleDisplay
//  Purpose:   Toggle the LCD display mode
//
//  Parms:     Direction (-1, 0, +1
//  Returns:   
//
static void fp_ToggleDisplay(int iDir)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   VLCD    *pstLcd=&pstMap->G_stVirtLcd;;

   iDir += (int)tFpMode;
   //
   if(iDir < 0)             iDir = NUM_FP_MODES-1;
   if(iDir >= NUM_FP_MODES) iDir = FPM_OFF;
   //
   tFpMode = (FPMODE) iDir;
   //
   switch(tFpMode)
   {
      default:
      case FPM_OFF:
         //
         // Display OFF
         //
         pstLcd->iLcdMode &= ~(MODE_BL_ON|MODE_DP_ON);
         if( sem_post(&pstMap->G_tSemLcd) == -1) LOG_Report(errno, "FPK", "fp_ToggleDisplay():sem_post");
         break;

      case FPM_LOGO:
         //
         // Display ON with Logo
         //
         fp_SetDisplayCoords(VLCD_ROW_MISC, VLCD_COL_MISC, MODE_BL_ON|MODE_DP_ON|MODE_CLEAR);
         fp_WriteVirtualLcd(VLCD_ROW_MISC1, VLCD_COL_MISC1, pcPowerup1);
         fp_WriteVirtualLcd(VLCD_ROW_MISC2, VLCD_COL_MISC2, pcPowerup2);
         if( sem_post(&pstMap->G_tSemLcd) == -1) LOG_Report(errno, "FPK", "fp_ToggleDisplay():sem_post");
         break;

      case FPM_TIME:
         //
         // Display Date and time
         //
         fp_SetDisplayCoords(VLCD_ROW_TAND, VLCD_COL_TAND, MODE_BL_ON|MODE_DP_ON|MODE_CLEAR);
         if( sem_post(&pstMap->G_tSemLcd) == -1) LOG_Report(errno, "FPK", "fp_ToggleDisplay():sem_post");
         break;

      case FPM_CMD_BOOT:
         //
         // Display Date and time
         //
         fp_SetDisplayCoords(VLCD_ROW_MISC, VLCD_COL_MISC, MODE_BL_ON|MODE_DP_ON|MODE_CLEAR);
         fp_WriteVirtualLcd(VLCD_ROW_MISC1, VLCD_COL_MISC1, pcExecCmnd);
         fp_WriteVirtualLcd(VLCD_ROW_MISC2, VLCD_COL_MISC2, pcCmndBoot);
         if( sem_post(&pstMap->G_tSemLcd) == -1) LOG_Report(errno, "FPK", "fp_ToggleDisplay():sem_post");
         break;

      case FPM_CMD_OFF:
         //
         // Display Date and time
         //
         fp_SetDisplayCoords(VLCD_ROW_MISC, VLCD_COL_MISC, MODE_BL_ON|MODE_DP_ON|MODE_CLEAR);
         fp_WriteVirtualLcd(VLCD_ROW_MISC1, VLCD_COL_MISC1, pcExecCmnd);
         fp_WriteVirtualLcd(VLCD_ROW_MISC2, VLCD_COL_MISC2, pcCmndPoff);
         if( sem_post(&pstMap->G_tSemLcd) == -1) LOG_Report(errno, "FPK", "fp_ToggleDisplay():sem_post");
         break;

      case FPM_CMD_EXIT:
         //
         // Display Date and time
         //
         fp_SetDisplayCoords(VLCD_ROW_MISC, VLCD_COL_MISC, MODE_BL_ON|MODE_DP_ON|MODE_CLEAR);
         fp_WriteVirtualLcd(VLCD_ROW_MISC1, VLCD_COL_MISC1, pcExecCmnd);
         fp_WriteVirtualLcd(VLCD_ROW_MISC2, VLCD_COL_MISC2, pcCmndExit);
         if( sem_post(&pstMap->G_tSemLcd) == -1) LOG_Report(errno, "FPK", "fp_ToggleDisplay():sem_post");
         break;
   }
   PRINTF3("fp_ToggleDisplay(%d,%d):Mode=%d" CRLF, pstLcd->ubVirtRow, pstLcd->ubVirtCol, tFpMode);
}

/*
 * Function    : fp_WriteVirtualLcd
 * Description : Write data into the virtual LCD
 *               
 * Parameters  : Row, col, string
 * Returns     : 
 *
 */
static void fp_WriteVirtualLcd(u_int8 ubRa, u_int8 ubCa, const char *pcText)
{
   int         iLen=0;
   int         iMaxLen, iAddr=0;
   RPIMAP     *pstMap=GLOBAL_GetMapping();
   VLCD       *pstLcd=&pstMap->G_stVirtLcd;

   if(pcText)
   {
      iAddr = (ubRa * LCD_VIRTUAL_COLS) + ubCa;
      iLen  = strlen(pcText);
      //
      // Check if the text fits into the virtual LCD
      //
      if( (ubRa >= LCD_VIRTUAL_ROWS) || (ubCa >= LCD_VIRTUAL_COLS) ) 
      {
         PRINTF3("fp_WriteVirtualLcd(%d,%d):BAD COORDS! [%s]" CRLF, ubRa, ubCa, pcText);
         return;
      }
      //
      // Write text into virtual LCD
      //
      iMaxLen = (LCD_VIRTUAL_ROWS * LCD_VIRTUAL_COLS) - iAddr;
      if(iMaxLen < iLen) 
      {
         PRINTF3("fp_WriteVirtualLcd():Too large string @%d (len=%d, max=%d)" CRLF, iAddr, iLen, iMaxLen);
         iLen = iMaxLen; 
      }
      //PRINTF5("fp_WriteVirtualLcd(%d,%d): Addr=%d, l=%d [%s]" CRLF, ubRa, ubCa, iAddr, iLen, pcText);
      ES_memcpy(&(pstLcd->pcVirtLcd[iAddr]), pcText, iLen);
   }
}


/* ======   Local Functions separator ===========================================
void ___KeyAction_functions(){}
==============================================================================*/

//
//  Function:  fp_DispExit
//  Purpose:   Exit the LCD display 
//
//  Parms:     Current state, current key 
//  Returns:   New state
//
static FPSTATE fp_DispExit(FPSTATE tState, u_int8 ubKey)
{
   RPIMAP       *pstMap=GLOBAL_GetMapping();
   const FPKEYH *pstKeyHdlr=&stFpKeyHandler[tState][ubKey];

   PRINTF2("fp_DispExit():State=%d, Key=%d" CRLF, tState, ubKey);
   tState = pstKeyHdlr->tNewState;
   PRINTF1("fp_DispExit():State=%d" CRLF, tState);
   //
   // We need to termine the timer thread else it will keep running forever
   // 
   if(pstMap->G_tVlcdPid) 
   {
      fp_KillProcess(pstMap->G_tVlcdPid, TRUE);
      pstMap->G_tVlcdPid = 0;
   }
   //
   // Give lcd thread time to cleanup
   //
   sleep(2);
   return(tState);
}

//
//  Function:  fp_DispInit
//  Purpose:   Initialize the FP LCD display
//
//  Parms:     Current state, current key 
//  Returns:   New state 
//
static FPSTATE fp_DispInit(FPSTATE tState, u_int8 ubKey)
{
   const FPKEYH *pstKeyHdlr=&stFpKeyHandler[tState][ubKey];
   RPIMAP       *pstMap=GLOBAL_GetMapping();
   VLCD         *pstLcd=&pstMap->G_stVirtLcd;

   PRINTF2("fp_DispInit():State=%d, Key=%d" CRLF, tState, ubKey);
   //
   pstMap->G_tVlcdPid = fp_CreateLcdHandler();
   pstLcd->iLcdMode |= MODE_LCD_ENABLED;
   //
   // We will go in STANDBY mode: make sure the Media Player threads are gone
   //
   tState = pstKeyHdlr->tNewState;
   fp_TerminateMedia();
   fp_ToggleDisplay(0);
   PRINTF1("fp_DispInit():State=%d" CRLF, tState);
   return(tState);
}

//
//  Function:  fp_DispTogL
//  Purpose:   Toggle the LCD display mode to the Left
//
//  Parms:     Current state, current key 
//  Returns:   New state
//
static FPSTATE fp_DispTogL(FPSTATE tState, u_int8 ubKey)
{
   const FPKEYH *pstKeyHdlr=&stFpKeyHandler[tState][ubKey];

   PRINTF2("fp_DispTogL():State=%d, Key=%d" CRLF, tState, ubKey);
   tState = pstKeyHdlr->tNewState;
   //
   // This is the DIM key: toggle through several LCD modes:
   //    o Display Off
   //    o Display Date & Time 
   //    o Display CPU options
   //
   fp_ToggleDisplay(-1);
   PRINTF1("fp_DispTogL():State=%d" CRLF, tState);
   return(tState);
}

//
//  Function:  fp_DispTogR
//  Purpose:   Toggle the LCD display mode to the Right
//
//  Parms:     Current state, current key 
//  Returns:   New state
//
static FPSTATE fp_DispTogR(FPSTATE tState, u_int8 ubKey)
{
   const FPKEYH *pstKeyHdlr=&stFpKeyHandler[tState][ubKey];

   PRINTF2("fp_DispTogR():State=%d, Key=%d" CRLF, tState, ubKey);
   tState = pstKeyHdlr->tNewState;
   //
   // This is the DIM key: toggle through several LCD modes:
   //    o Display Off
   //    o Display Date & Time 
   //    o Display CPU options
   //
   fp_ToggleDisplay(+1);
   PRINTF1("fp_DispTogR():State=%d" CRLF, tState);
   return(tState);
}

//
//  Function:  fp_DispExec
//  Purpose:   Execute the current LCD display mode
//
//  Parms:     Current state, current key 
//  Returns:   New state
//
static FPSTATE fp_DispExec(FPSTATE tState, u_int8 ubKey)
{
   RPIMAP       *pstMap=GLOBAL_GetMapping();
   const FPKEYH *pstKeyHdlr=&stFpKeyHandler[tState][ubKey];

   PRINTF2("fp_DispExec():State=%d, Key=%d" CRLF, tState, ubKey);
   tState = pstKeyHdlr->tNewState;
   //
   // This is the ENTER key: execute the currently selected display mode (If any defined)
   //    o Reboot 
   //    o Power Off
   //    o Exit PopKodi
   //
   //
   switch(tFpMode)
   {
      default:
         break;

      case FPM_CMD_BOOT:
         //
         // Reboot 
         //
         PRINTF("fp_DispExec():** SYSTEM REBOOT **" CRLF);
         fp_StartProcess(pcExecBoot, pcParmNone, pcExecBoot, 0);
         break;

      case FPM_CMD_OFF:
         //
         // Power Off
         //
         PRINTF("fp_DispExec():** SYSTEM POWER OFF **" CRLF);
         fp_StartProcess(pcExecPoff, pcParmNone, pcExecPoff, 0);
         break;

      case FPM_CMD_EXIT:
         //
         // Exit PopKodi
         //
         PRINTF("fp_DispExec():** POPKODI EXIT **" CRLF);
         fp_SetDisplayCoords(VLCD_ROW_MISC, VLCD_COL_MISC, MODE_CLEAR);
         if( sem_post(&pstMap->G_tSemLcd) == -1) LOG_Report(errno, "FPK", "fp_ToggleDisplay():sem_post");
         //
         // Give lcd thread time to cleanup
         //
         sleep(1);
         tState = FPS_EXIT;
         break;
   }
   PRINTF1("fp_DispExec():State=%d" CRLF, tState);
   return(tState);
}

//
//  Function:  fp_KodiExit
//  Purpose:   Exit the Kodi media player
//
//  Parms:     Current state, current key 
//  Returns:   New state
//
static FPSTATE fp_KodiExit(FPSTATE tState, u_int8 ubKey)
{
   RPIMAP       *pstMap=GLOBAL_GetMapping();
   VLCD         *pstLcd=&pstMap->G_stVirtLcd;
   const FPKEYH *pstKeyHdlr=&stFpKeyHandler[tState][ubKey];

   PRINTF2("fp_KodiExit():State=%d, Key=%d" CRLF, tState, ubKey);
   //
   // Request the Media player modules to exit:
   //    o lcdproc
   //    o LCDd
   //    o Display ON while Kodi exit
   //    o kodi-bin
   //
   if( (tPidLcdp = fp_GetPidByName(pcPathLcdp)) > 0)  fp_KillProcess(tPidLcdp, TRUE);
   sleep(1);
   if( (tPidLcdd = fp_GetPidByName(pcPathLcdd)) > 0)  fp_KillProcess(tPidLcdd, TRUE);
   sleep(1);
   if( (tPidKodi = fp_GetPidByName(pcPathKodi)) > 0)  fp_KillProcess(tPidKodi, TRUE);
   //
   // Enable LCD for our usage again
   //
   pstLcd->iLcdMode |= MODE_LCD_ENABLED;
   fp_SetDisplayCoords(VLCD_ROW_MISC, VLCD_COL_MISC, MODE_BL_ON|MODE_DP_ON|MODE_CLEAR);
   fp_WriteVirtualLcd(VLCD_ROW_MISC1, VLCD_COL_MISC1, pcExitKodi);
   fp_WriteVirtualLcd(VLCD_ROW_MISC2, VLCD_COL_MISC2, pcPowerup2);
   if( sem_post(&pstMap->G_tSemLcd) == -1) LOG_Report(errno, "FPK", "fp_KodiExit():sem_post");
   //
   sleep(2);
   fp_ToggleDisplay(0);
   //
   tState = pstKeyHdlr->tNewState;
   PRINTF1("fp_KodiExit():State=%d" CRLF, tState);
   //
   return(tState);
}

//
//  Function:  fp_KodiInit
//  Purpose:   Startup the Kodi media player
//
//  Parms:     Current state, current key 
//  Returns:   New state
//
static FPSTATE fp_KodiInit(FPSTATE tState, u_int8 ubKey)
{
   pid_t         tPid;
   RPIMAP       *pstMap=GLOBAL_GetMapping();
   VLCD         *pstLcd=&pstMap->G_stVirtLcd;
   const FPKEYH *pstKeyHdlr=&stFpKeyHandler[tState][ubKey];

   PRINTF2("fp_KodiInit():State=%d, Key=%d" CRLF, tState, ubKey);
   //
   // Startup the Media player modules:
   //    o Display ON while Kodi startup
   //    o kodi-bin
   //    o LCDd
   //    o lcdproc
   //
   fp_SetDisplayCoords(VLCD_ROW_MISC, VLCD_COL_MISC, MODE_BL_ON|MODE_DP_ON|MODE_CLEAR);
   fp_WriteVirtualLcd(VLCD_ROW_MISC1, VLCD_COL_MISC1, pcDispKodi);
   fp_WriteVirtualLcd(VLCD_ROW_MISC2, VLCD_COL_MISC2, pcPowerup2);
   if( sem_post(&pstMap->G_tSemLcd) == -1) LOG_Report(errno, "FPK", "fp_KodiInit():sem_post");
   //
   fp_TerminateMedia();
   //
   // The returned PIDs could be of either the startup shell or thr actual 
   // process itself. Ignore them here and retrieve the running PIDs when
   // we actually need them
   //
   tPid = fp_StartProcess(pcCmndKodi, pcParmKodi, pcPathKodi, 30);
   if(tPid) 
   {
      //
      // kodi is starting up
      // Disable LCD screen for our usage
      // startup LCD deamons
      //
      pstLcd->iLcdMode &= ~MODE_LCD_ENABLED;
      fp_StartProcess(pcCmndLcdd, pcParmLcdd, pcPathLcdd, 5);
      fp_StartProcess(pcCmndLcdp, pcParmLcdp, pcPathLcdp, 5);
      tState = pstKeyHdlr->tNewState;
      PRINTF1("fp_KodiInit():State=%d" CRLF, tState);
   }
   else
   {
      PRINTF1("fp_KodiInit():ERROR: %s did not start!" CRLF, pcPathKodi);
      LOG_Report(errno, "FPK", "fp_KodiInit():ERROR: %s did not start!", pcPathKodi);
   }
   //
   return(tState);
}

//
//  Function:  fp_NoAction
//  Purpose:   Do nothing with this key in this state
//
//  Parms:     Current state, current key 
//  Returns:   Same state 
//
static FPSTATE fp_NoAction(FPSTATE tState, u_int8 ubKey)
{
   PRINTF2("fp_NoAction():State=%d, Key=%d" CRLF, tState, ubKey);
   return(tState);
}

/*------  Local functions separator -----------------------------------------
__LCD_Thread_functions_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : lcd_EnableTimer
 * Description : Enable the CMND thread timer
 *
 * Parameters  : Sidnal, mSecs, mode
 * Returns     : Timer ID
 *
 */
static timer_t lcd_EnableTimer(int iSigNr, int iMsec, int mode)
{
   struct sigevent   stSigEvt;
   timer_t           tId;
   struct itimerspec stItVal;
   struct itimerspec StOfVal;
 
   // Create the POSIX timer to generate signo
   stSigEvt.sigev_notify          = SIGEV_SIGNAL;
   stSigEvt.sigev_signo           = iSigNr;
   stSigEvt.sigev_value.sival_ptr = &tId;
 
   if (timer_create(CLOCK_REALTIME, &stSigEvt, &tId) == 0) 
   {
      stItVal.it_value.tv_sec  = iMsec / 1000;
      stItVal.it_value.tv_nsec = (long)(iMsec % 1000) * (1000000L);
 
      if(mode == 1) 
      {
         stItVal.it_interval.tv_sec  = stItVal.it_value.tv_sec;
         stItVal.it_interval.tv_nsec = stItVal.it_value.tv_nsec;
      } 
      else 
      {
         stItVal.it_interval.tv_sec  = 0;
         stItVal.it_interval.tv_nsec = 0;
      }
      if (timer_settime(tId, 0, &stItVal, &StOfVal) != 0) 
      {
         LOG_Report(errno, "FPK", "lcd_EnableTimer():  timer_settime() error");
      }
   } 
   else 
   {
      LOG_Report(errno, "FPK", "lcd_EnableTimer(): timer_create() error");
      return((timer_t)-1);
   }
   return(tId);
}

//
//  Function:  lcd_HandleLcd
//  Purpose:   Handle the update of the actual LCD module
//
//  Parms:     
//  Returns:   If killed
//  Note:      The Virtual LCD screen holds the actual LCD image. 
//             Refresh the LCD lines from this image
//             
//             
//
static void lcd_HandleLcd(void)
{
   int      iCc;
   sigset_t tBlockset;
   char    *pcLine1;
   char    *pcLine2;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   VLCD    *pstLcd=&pstMap->G_stVirtLcd;

   fTmrRunning = TRUE;
   PRINTF1("lcd_HandleLcd(): Child: Flag=%p" CRLF, &fTmrRunning);
   //
   // This is the new lcd thread
   //
   if(lcd_SignalRegister(&tBlockset) == FALSE) exit(-1);
   //
   lcd_SetTimer(1000);
   if(sem_init(&pstMap->G_tSemLcd, 1, 0) == -1) LOG_Report(errno, "FPK", "fp_CreateLcdHandler():sem_init");
   //
   while(fTmrRunning)
   {
      if(pstLcd->iLcdMode & MODE_LCD_ENABLED)
      {
         pcLine1=&pstLcd->pcVirtLcd[((pstLcd->ubVirtRow+0)*LCD_VIRTUAL_COLS)+pstLcd->ubVirtCol];
         pcLine2=&pstLcd->pcVirtLcd[((pstLcd->ubVirtRow+1)*LCD_VIRTUAL_COLS)+pstLcd->ubVirtCol];
         //
         PRINTF3("lcd_HandleLcd(%d,%d):Enabled LCD m=0x%x" CRLF, pstLcd->ubVirtRow, pstLcd->ubVirtCol, pstLcd->iLcdMode);
         FP_SHOWVIRTIALSCREEN();
         //
         // Update the LCD on a signal from the LCD semaphore
         //
         if(pstLcd->iLcdMode & MODE_DP_ON)   LCD_Mode(LCD_MODE_DISPLAY_ON);
         else                                LCD_Mode(LCD_MODE_DISPLAY_OFF);
         //
         if(pstLcd->iLcdMode & MODE_BL_ON)   LCD_Mode(LCD_MODE_BACKLIGHT_ON);
         else                                LCD_Mode(LCD_MODE_BACKLIGHT_OFF);
         //
         if(pstLcd->iLcdMode & MODE_CLEAR)   
         {
            LCD_ClearScreen();
            pstLcd->iLcdMode &= ~MODE_CLEAR;
         }
         //
         LCD_Write(0, 0, (u_int8 *)pcLine1, LCD_ACTUAL_COLS);
         LCD_Write(1, 0, (u_int8 *)pcLine2, LCD_ACTUAL_COLS);
      }
      else
      {
         PRINTF3("lcd_HandleLcd(%d,%d):DISABLED LCD m=0x%x" CRLF, pstLcd->ubVirtRow, pstLcd->ubVirtCol, pstLcd->iLcdMode);
      }
      while ((iCc = sem_wait(&pstMap->G_tSemLcd)) == -1 && errno == EINTR)
      {
         //PRINTF3("lcd_HandleLcd(): iCc=%d, errno=%d (%s)" CRLF, iCc, errno, strerror(errno));
         continue;       /* Restart if interrupted by handler */ 
      }
   }
   PRINTF("lcd_HandleLcd():Exit" CRLF);
}

//
//  Function:   lcd_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void lcd_ReceiveSignalInt(int iSignal)
{
   fTmrRunning = FALSE;
   PRINTF1("lcd_ReceiveSignalInt(): Child: Flag=%p" CRLF, &fTmrRunning);
   LOG_Report(0, "FPK", "lcd_ReceiveSignalInt()");
}

//
//  Function:   lcd_ReceiveSignalUser1
//  Purpose:    Add the SIGUSR1 command in the buffer
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void lcd_ReceiveSignalUser1(int iSignal)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   //
   // Trigger LCD update
   //
   if( sem_post(&pstMap->G_tSemLcd) == -1) LOG_Report(errno, "FPK", "lcd_ReceiveSignalUser1():sem_post");
   LOG_Report(0, "FPK", "lcd_ReceiveSignalUser1()");
}

//
//  Function:   lcd_ReceiveSignalUser2
//  Purpose:    Add the SIGUSR2 command in the buffer
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void lcd_ReceiveSignalUser2(int iSignal)
{
   LOG_Report(0, "FPK", "lcd_ReceiveSignalUser2()");
}

//
//  Function:   lcd_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void lcd_ReceiveSignalTerm(int iSignal)
{
   fTmrRunning = FALSE;
   PRINTF1("lcd_ReceiveSignalTerm(): Child: Flag=%p" CRLF, &fTmrRunning);
   LOG_Report(0, "FPK", "lcd_ReceiveSignalTerm()");
}

/*
 * Function    : lcd_SetTimer
 * Description : Setup a timer
 *
 * Parameters  : Timer value (msecs)
 * Returns     : 
 *
 */
static void lcd_SetTimer(int iTime)
{
   struct sigaction sigact;
 
   sigemptyset(&sigact.sa_mask);
   sigact.sa_flags = SA_SIGINFO;
   sigact.sa_sigaction = lcd_TimerHandler;
 
   // Set up sigaction to catch signal
   if (sigaction(SIGRTMAX, &sigact, NULL) == -1) 
   {
      LOG_Report(errno, "FPK", "lcd_SetTimer(): sigaction failed");
      return;
   }
   tTimerId = lcd_EnableTimer(SIGRTMAX, iTime, 1);
}

//
//  Function:   lcd_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool lcd_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

   if( signal(SIGUSR1, &lcd_ReceiveSignalUser1) == SIG_ERR)
   {
      LOG_Report(errno, "FPK", "lcd_SignalRegister(): SIGUSR1 Error");
      fCC = FALSE;
   }
   if( signal(SIGUSR2, &lcd_ReceiveSignalUser2) == SIG_ERR)
   {
      LOG_Report(errno, "FPK", "lcd_SignalRegister(): SIGUSR2 Error");
      fCC = FALSE;
   }
  // CTL-X handler
  if( signal(SIGTERM, &lcd_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "FPK", "lcd_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &lcd_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "FPK", "lcd_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
  }
  return(fCC);
}

/*
 * Function    : lcd_TimerHandler
 * Description : Timer runs every Sec: update the golbal timer counter and signal the LCD
 *               handler
 * Parameters  : 
 * Returns     : 
 *
 */
static void lcd_TimerHandler(int iSigNr, siginfo_t *pstInfo, void *pvContext)
{
   u_int32  ulSecs;
   char     cText[30];
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   GLOBAL_UpdateSecs();
   //
   if(--iTimePerSec <= 0)
   {
      //
      // Time to update the clock
      //
      iTimePerSec = iTimePerSecDef;
      ulSecs = RTC_GetSystemSecs();
      //
      RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY, cText, ulSecs);
      //PRINTF1("lcd_TimerHandler():Date[%s]" CRLF, cText);
      fp_WriteVirtualLcd(VLCD_ROW_DATE, VLCD_COL_DATE, (const char *)cText);
      //
      if(iTimePerSec < 60)  RTC_ConvertDateTime(TIME_FORMAT_HH_MM_SS, cText, ulSecs);
      else                  RTC_ConvertDateTime(TIME_FORMAT_HH_MM,    cText, ulSecs);
      fp_WriteVirtualLcd(VLCD_ROW_TIME, VLCD_COL_TIME, (const char *)cText);
      //PRINTF1("lcd_TimerHandler(): Time[%s]" CRLF, cText);
      //
      // Trigger LCD update
      //
      if( sem_post(&pstMap->G_tSemLcd) == -1) LOG_Report(errno, "FPK", "lcd_TimerHandler():sem_post");
   }
}


