CC      = gcc
DEFS    = 
CFLAGS  = $(DEFS) -O2 -Wall -g
INCDIR  =
LDFLGS 	= -pthread
DESTDIR = ./bin
LIBFLGS	= -L/usr/local/lib -L/usr/lib
LIBS    = -lrt -llirc_client -lwiringPi
OBJ     = main.o rpilcd.o safecalls.o eos_api.o log.o rtc.o misc_api.o globals.o \
		  rpircu.o emulip.o netfiles.o httppage.o dynpage.o raspjson.o fpkeys.o

all: modules $(DESTDIR)/popkodi

modules:
$(DESTDIR)/popkodi: $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LIBFLGS) $(LIBS) $(LDFLGS)
	strip $(DESTDIR)/popkodi

sync:
	cp -u /mnt/rpi/softw/popkodi/*.c ./
	cp -u /mnt/rpi/softw/popkodi/*.h ./
	cp -u /mnt/rpi/softw/popkodi/*.conf ./
	cp -u /mnt/rpi/softw/popkodi/makefile ./
	cp -u /mnt/rpi/softw/popkodi/popkod ./

forcesync:
	cp /mnt/rpi/softw/popkodi/*.c ./
	cp /mnt/rpi/softw/popkodi/*.h ./
	cp /mnt/rpi/softw/popkodi/*.conf ./
	cp /mnt/rpi/softw/popkodi/makefile ./
	cp /mnt/rpi/softw/popkodi/popcod ./

install:
	sudo /etc/init.d/popkod stop
	sudo cp bin/popkodi /usr/sbin/popkodi
	sudo /etc/init.d/popkod restart

restart:
	sudo /etc/init.d/popkod stop
	sudo /etc/init.d/popkod restart

ziplog:
	sudo gzip -c /usr/local/share/rpi/popkodi.log >>/usr/local/share/rpi/popkodilogs.gz
	sudo rm /usr/local/share/rpi/popkodi.log
	sudo rm /mnt/rpicache/popkodi.log

clean:
	rm -rf *.o
	rm -rf $(DESTDIR)/popkodi

.c.o:
	$(CC) -c $(CFLAGS) $(INCDIR) $< 
