/*  (c) Copyright:  2013 Patrn, Confidential Data
**
**  $Workfile:   $      raspjson.c
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            Raspbian JSON functions and data
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       27 Jun 2013
**
 *  Revisions:
 *    $Log:   $
 *       cr308:   Fix JSON comma error
 *
 *
 *
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "echotype.h"
#include "dynpage.h"
#include "httppage.h"
#include "log.h"
#include "globals.h"
#include "raspjson.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static RPIJSON *json_AllocateObject       (int);
static void     json_FreeObject           (RPIJSON *);
static RPIJSON *json_Insert               (RPIJSON *, const char *);



static char pcHttpResponseHeaderJson[] = 
               "HTTP/1.1 200 OK" NEWLINE 
               "Content-Type: application/json; charset=UTF-8" NEWLINE
               "Content-Length: %d" NEWLINE
                NEWLINE;


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : JSON_RespondObject
 * Description : Raspicam Respond with the JSON objects
 *
 * Parameters  : Client, JSON object
 * Returns     : TRUE if OKee
 *
 */
bool JSON_RespondObject(NETCL *pstCl, RPIJSON *pstObj)
{
   PRINTF2("JSON_RespondObject(): L=%d (Act=%d)" CRLF, pstObj->iIdx, strlen(pstObj->pcObject));
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeaderJson, pstObj->iIdx);
   HTTP_BuildGeneric(pstCl, pstObj->pcObject);
   return(TRUE);
}

/*
 * Function    : JSON_InsertParameter
 * Description : Insert parameter into JSON object
 *
 * Parameters  : JSON object, parameter, value, style (multi/text/int
 * Returns     : JSON object
 *
 */
RPIJSON *JSON_InsertParameter(RPIJSON *pstObj, const char *pcParm, char *pcValue, int iStyle)
{
   if(pstObj == NULL)
   {
      pstObj = json_AllocateObject(JSON_DEFAULT_OBJECT_SIZE);
   }
   //
   PRINTF3("JSON_InsertParameter():parm=%s, value=%s, style=%d" CRLF, pcParm, pcValue, iStyle);
   //
   if(iStyle & JE_CURLB)   pstObj = json_Insert(pstObj, "{");
                           pstObj = json_Insert(pstObj, "\"");
                           pstObj = json_Insert(pstObj, pcParm);
                           pstObj = json_Insert(pstObj, "\"");
                           pstObj = json_Insert(pstObj, ":");
   if(iStyle & JE_SQRB)  { pstObj->iLastComma = -1;              pstObj = json_Insert(pstObj, "[\r\n"); }
   if(iStyle & JE_TEXT)    pstObj = json_Insert(pstObj, "\"");
                           pstObj = json_Insert(pstObj, pcValue);
   if(iStyle & JE_TEXT)    pstObj = json_Insert(pstObj, "\"");
   if(iStyle & JE_COMMA) { pstObj->iLastComma = pstObj->iIdx;    pstObj = json_Insert(pstObj, ", ");     }
   if(iStyle & JE_SQRE)  { pstObj->iLastComma = pstObj->iIdx +1; pstObj = json_Insert(pstObj, "],\r\n"); }
   if(iStyle & JE_CURLE) { pstObj->iLastComma = pstObj->iIdx +1; pstObj = json_Insert(pstObj, "},\r\n"); }
   if(iStyle & JE_CRLF)    pstObj = json_Insert(pstObj, "\r\n");
   //
   return(pstObj);
}

/*
 * Function    : JSON_InsertValue
 * Description : Insert single value into JSON object
 *
 * Parameters  : JSON object, value, style (multi/text/int
 * Returns     : JSON object
 *
 */
RPIJSON *JSON_InsertValue(RPIJSON *pstObj, char *pcValue, int iStyle)
{
   if(pstObj == NULL)
   {
      pstObj = json_AllocateObject(JSON_DEFAULT_OBJECT_SIZE);
   }
   //
   PRINTF3("JSON_InsertParameter():parm=%s, value=%s, style=%d" CRLF, pcParm, pcValue, iStyle);
   //
   if(iStyle & JE_CURLB)   pstObj = json_Insert(pstObj, "{");
   if(iStyle & JE_SQRB)    pstObj = json_Insert(pstObj, "[\r\n");
   if(iStyle & JE_TEXT)    pstObj = json_Insert(pstObj, "\"");
                           pstObj = json_Insert(pstObj, (const char *)pcValue);
   if(iStyle & JE_TEXT)    pstObj = json_Insert(pstObj, "\"");
   if(iStyle & JE_COMMA) { pstObj->iLastComma = pstObj->iIdx;    pstObj = json_Insert(pstObj, ", ");     }
   if(iStyle & JE_SQRE)  { pstObj->iLastComma = pstObj->iIdx +1; pstObj = json_Insert(pstObj, "],\r\n"); }
   if(iStyle & JE_CURLE) { pstObj->iLastComma = pstObj->iIdx +1; pstObj = json_Insert(pstObj, "},\r\n"); }
   if(iStyle & JE_CRLF)    pstObj = json_Insert(pstObj, "\r\n");
   //
   return(pstObj);
}

/*
 * Function    : JSON_TerminateObject
 * Description : Terminate JSON object
 *
 * Parameters  : JSON object
 * Returns     : JSON Object
 *
 */
RPIJSON *JSON_TerminateObject(RPIJSON *pstObj)
{
   if(pstObj->iLastComma > 0)
   {
      pstObj->pcObject[pstObj->iLastComma] = ' ';
      pstObj->iLastComma = -1;
   }
   pstObj = json_Insert(pstObj, "\r\n}\r\n");

   PRINTF("JSON_TerminateObject()" CRLF);
   //PRINTF1("JSON_TerminateObject():%s" CRLF, pstObj->pcObject);
   return(pstObj);
}

/*
 * Function    : JSON_TerminateArray
 * Description : Build JSON array
 *
 * Parameters  : JSON object
 * Returns     : JSON Object
 *
 */
RPIJSON *JSON_TerminateArray(RPIJSON *pstObj)
{
   if(pstObj->iLastComma > 0)
   {
      pstObj->pcObject[pstObj->iLastComma] = ' ';
      pstObj->iLastComma = -1;
   }
   pstObj = json_Insert(pstObj, "\r\n],\r\n");
   return(pstObj);
}

/*
 * Function    : JSON_GetObjectSize
 * Description : return JSON object size
 *
 * Parameters  : JSON object
 * Returns     : Size
 *
 */
int JSON_GetObjectSize(RPIJSON *pstObj)
{
   PRINTF1("JSON_GetObjectSize():%d" CRLF, pstObj->iIdx);
   return(pstObj->iIdx);
}

/*
 * Function    : JSON_ReleaseObject
 * Description : Release JSON objects
 *
 * Parameters  : JSON object
 * Returns     : 
 *
 */
void JSON_ReleaseObject(RPIJSON *pstObj)
{
   PRINTF("JSON_ReleaseObject()" CRLF);
   //
   json_FreeObject(pstObj);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : json_AllocateObject
 * Description : Build empty JSON object
 *
 * Parameters  : Initial size
 * Returns     : JSON object
 *
 */
static RPIJSON *json_AllocateObject(int iLength)
{
   RPIJSON *pstObj = safemalloc(sizeof(RPIJSON));

   pstObj->pcObject     = safemalloc(iLength);
   pstObj->iIdx         = 0;
   pstObj->iLastComma   = -1;
   pstObj->iSize        = iLength;
   pstObj->iFree        = iLength;
   
   PRINTF1("json_AllocateObject():%d" CRLF, iLength);
   return(pstObj);
}

/*
 * Function    : json_FreeObject
 * Description : Free JSON object
 *
 * Parameters  : JSON Object
 * Returns     : 
 *
 */
static void json_FreeObject(RPIJSON *pstObj)
{
   PRINTF2("json_FreeObject():%d bytes used, %d bytes free" CRLF, pstObj->iIdx, pstObj->iFree);
   //
   if(pstObj)
   {
      safefree(pstObj->pcObject);
      safefree(pstObj);
   }
}

/*
 * Function    : json_Insert
 * Description : Insert JSON object
 *
 * Parameters  : JSON Object, data
 * Returns     : JSON Object (might have changed)
 *
 */
static RPIJSON *json_Insert(RPIJSON *pstObj, const char *pcData)
{
   RPIJSON *pstNew = pstObj;

   int   iIdx=pstNew->iIdx;
   int   iLen=strlen(pcData);

   //PRINTF1("json_Insert():[%s]" CRLF, pcData);
   //
   if(iLen >= pstNew->iFree)
   {
      PRINTF2("json_Insert(): Need more space: size=%d, free=%d" CRLF, iLen, pstNew->iFree);
      pstNew = json_AllocateObject(pstObj->iSize + JSON_DEFAULT_OBJECT_SIZE);
      strncpy(pstNew->pcObject, pstObj->pcObject, pstObj->iSize);
      // Copy new values or necessary values from old object
      pstNew->iIdx       = iIdx;
      pstNew->iFree      = pstNew->iFree - iIdx;
      pstNew->iLastComma = pstObj->iLastComma;
      json_FreeObject(pstObj);
   }
   strncpy(&pstNew->pcObject[iIdx], pcData, pstNew->iFree);
   pstNew->iIdx  += iLen;
   pstNew->iFree -= iLen;
   //
   return(pstNew);
}


