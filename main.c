/*  (c) Copyright:  2015 Patrn, Confidential Data
**
**  $Workfile:   $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:            popkodi handles communication with the Kodi media player
**
**  Entry Points:       main()
**
**
**
**
**
**
**
**
**
**
**
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       28 Oct 2015
**
 *  Revisions:
 *    $Log:   $
 * v100: Initial revision.
 *
 *
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <resolv.h>

#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include "echotype.h"
#include "config.h"
#include "globals.h"
#include "rtc.h"
#include "log.h"
#include "misc_api.h"
#include "eos_api.h"
#include "safecalls.h"
#include "rpircu.h"
#include "rpilcd.h"
#include "fpkeys.h"

//#define USE_PRINTF
#include "printf.h"


#if defined(__DATE__) && defined(__TIME__)
   static const char RPI_BUILT[] = __DATE__ " " __TIME__;
#else
   static const char RPI_BUILT[] = "Unknown";
#endif

static pid_t   rpi_Deamon              (void);
static void    rpi_Help                (void);
static int     rpi_KillProcess         (pid_t, bool);
static void    rpi_Version             (void);
//
static bool    rpi_SignalRegister      (sigset_t *);
static void    rpi_ReceiveSignalInt    (int);
static void    rpi_ReceiveSignalTerm   (int);
static void    rpi_ReceiveSignalUser1  (int);
static int     rpi_WaitForKey          (int);

#ifdef FEATURE_DUMP_DATE
static void    rpi_Dump                (char *, int, bool);
#define RPI_DUMP(x,y,z)                rpi_Dump(x,y,z)
#else    //FEATURE_DUMP_DATE
#define RPI_DUMP(x,y,z)
#endif   //FEATURE_DUMP_DATE
//
static bool    fRpiRunning = TRUE;
static FPSTATE tFpState    = FPS_NONE;
static sem_t   tSemKey;

/* ======   Local Functions separator ===========================================
___MAIN_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   main
//  Purpose:    Main entry point for the Raspberry Pi Kodi monitor
//
//  Parms:      Commandline options
//  Returns:    Exit codes
//
int main(int argc, char **argv)
{
   int         iOpt, iCc=254;
   sigset_t    tBlockset;
   RPIMAP     *pstMap;
   u_int8      ubKey;

   if( LOG_ReportOpenFile(RPI_PUBLIC_LOG_FILE) == FALSE)
   {
      LOG_Report(errno, "POP", "main():Log file open error");
      ES_printf("POP: !----------------------------!" CRLF);
      ES_printf("POP: !! Using STDERR as log-file !!" CRLF);
      ES_printf("POP: !----------------------------!" CRLF);
   }
   else
   {
      ES_printf("PopKodi():Logfile is %s" CRLF, RPI_PUBLIC_LOG_FILE);
   }
   //
   // Use a semaphore for FP key arrival
   //
   if(sem_init(&tSemKey, 1, 0) == -1) LOG_Report(errno, "POP", "main():sem_init");
   //
   pstMap = GLOBAL_Init();
   //
   GLOBAL_DUMPVARS("PopKodi");
   //
   if(rpi_SignalRegister(&tBlockset) == FALSE) exit(iCc);
   //
   while ((iOpt = getopt(argc, argv, "DFhv")) != -1)
   {
      switch (iOpt)
      {
         case 'D':
            PRINTF("main():Option -D" CRLF);
            ES_printf("Starting popkodi as deamon" CRLF);
            stderr = fopen(RPI_ERROR_FILE, "a+");
            LOG_Report(0, "POP", "main():Option -D: popkodi deamon (stderr rerouted).");
            rpi_Deamon();
            //
            // Deamon mode:
            //    the HOST has exited already and we are now SRVR and running in the background
            //
            break;

         case 'F':
            PRINTF("PopKodi():Option -F" CRLF);
            break;

         case 'v':
            PRINTF("PopKodi():Option -v"CRLF);
            rpi_Version() ;
            fRpiRunning = FALSE;
            break;
   
         case 'h':
            PRINTF("PopKodi():Option -h"CRLF);
            rpi_Help();
            fRpiRunning = FALSE;
            break;
   
         default:
            PRINTF("PopKodi(): Invalid option."CRLF);
            rpi_Help();
            fRpiRunning = FALSE;
            break;
      }
   }
   //
   // Wait for data
   //
   if(fRpiRunning)
   {
      ES_printf("%s : Build:%s" CRLF, pstMap->G_pcVersionSw, RPI_BUILT);
      LOG_Report(0, "POP", "%s : Build:%s", pstMap->G_pcVersionSw, RPI_BUILT);
      //
      RCU_Open();
      FP_KeyInit();
      //
      tFpState = FP_KeyHandler(FPS_NONE, RCU_NOKEY);
      //
      // Enter main loop
      //
      while(fRpiRunning)
      {
         switch( rpi_WaitForKey(10) )
         {
            case 0:
               //
               // We have a key: process it
               //
               ubKey    = pstMap->G_iFrontPanelKey;
               tFpState = FP_KeyHandler(tFpState, ubKey);
               if(tFpState == FPS_EXIT) fRpiRunning = FALSE;
               pstMap->G_iFrontPanelKey = RCU_NOKEY;
               break;

            default:
            case -1:
               //
               // Timeout on FP key wait
               //
               PRINTF1("main(): no FP Key yet(errno=%d)" CRLF, errno);
               break;
         }
      }
      FP_KeyHandler(FPS_EXIT, RCU_NOKEY);
      FP_KeyExit();
   }
   //
   // Cleanup background threads
   //
   if( sem_destroy(&tSemKey) == -1) LOG_Report(errno, "POP", "main():sem_destroy");
   rpi_KillProcess(pstMap->G_tVlcdPid, TRUE);
   rpi_KillProcess(pstMap->G_tLircPid, TRUE);
   //
   // Cleanup
   //
   GLOBAL_Sync();
   GLOBAL_Close();
   //
   LOG_Report(0, "POP", "Version=%s now EXIT", VERSION);
   LOG_ReportCloseFile();
   //
   ES_printf("PopKodi():exit(%d)" CRLF, iCc);
   exit(iCc);
}

/* ======   Local Functions separator ===========================================
___LOCAL_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   rpi_Deamon
//  Purpose:    Run the Raspberry Pi Media Player Monitor as a deamon, rather than the CLI version
//
//  Parms:
//  Returns:
//
static pid_t rpi_Deamon(void)
{
   pid_t    tPid;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent or HOST: exit here
         pstMap->G_tHostPid = 0;
         exit(0);
         break;

      case 0:
         // Child or SRVR: take over and keep running in the background
         pstMap->G_tSrvrPid = getpid();
         PRINTF1("rpi-Deamon(): popkodi running background mode, SRVR(%d)" CRLF, pstMap->G_tSrvrPid);
         LOG_Report(0, "POP", "rpi-Deamon(): popkodi running background mode, SRVR(%d)", pstMap->G_tSrvrPid);
         break;

      case -1:
         // Error
         ES_printf("rpi-Deamon(): Error!"CRLF);
         LOG_Report(errno, "POP", "rpi-Deamon(): Error");
         exit(255);
         break;
   }
   return(tPid);
}

/*
 * Function    : rpi_KillProcess
 * Description : Kill an ongoing process (if any)
 *
 * Parameters  : pid to kill, friendly YESNO
 * Returns     : completion code (0=ok)
 *
 */
static int rpi_KillProcess(pid_t tPid, bool fFriendly)
{
   int      iCc=0;
   pid_t    tPidAct;
   int      iStatus;
   char     cShell[32];

   if(tPid)
   {
      //
      // Ongoing action: terminate friendly or forced
      //
      if(fFriendly)
      {
         kill(tPid, SIGTERM);
         //
         //LOG_Report(0, "POP", "rpi_KillProcess(): Friendly ask background thread pid=%d to exit", tPid);
         PRINTF1("rpi_KillProcess(): Friendly ask background thread pid=%d to exit" CRLF, tPid);
      }
      else
      {
         LOG_Report(0, "POP", "rpi_KillProcess(): Terminate background thread (pid=%d)", tPid);
         ES_printf("rpi_KillProcess(): Kill background thread: pid=%d" CRLF, tPid);
         ES_sprintf(cShell, "kill -kill %d", tPid);
         iCc  = system(cShell);
         while( ((tPidAct = waitpid(tPid, &iStatus, 0)) == -1) && (errno == EINTR) )
           continue;       /* Restart if interrupted by handler */ 
         //
         ES_printf("rpi_KillProcess(): Kill background thread: pid=%d, Done (CC=%d)" CRLF, tPidAct, iCc);
      }
   }
   return(iCc);
}

//
//  Function:   rpi_Version
//  Purpose:    Show the version string
//
//  Parms:
//  Returns:
//
static void rpi_Version(void)
{
  char  cLog[DATE_TIME_SIZE];

  RTC_GetDateTimeSecs(cLog);

  ES_printf(CRLF);
  ES_printf("Raspberry Pi: KODI Media Player monitor v%s" CRLF, VERSION);
  ES_printf("   Time  : %s" CRLF, cLog);
  ES_printf("   Built : %s" CRLF, RPI_BUILT);
  ES_printf("   Home  : http://www.patrn.nl" CRLF);
  ES_printf(CRLF);

  fflush(stdout);
}

//
//  Function:   rpi_Help
//  Purpose:
//
//  Parms:
//  Returns:
//
static void rpi_Help(void)
{
   rpi_Version();
   //
   ES_printf("Usage : PopKodi [-h] [-v] " CRLF);
   ES_printf(CRLF);
   ES_printf("Available options:" CRLF);
   ES_printf("  -h\t\tThis help" CRLF);
   ES_printf("  -v\t\tShow version number" CRLF);
   ES_printf(CRLF);
}

//
//  Function:   rpi_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool rpi_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // Local signals :
  if( signal(SIGUSR1, &rpi_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "POP", "rpi_SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &rpi_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "POP", "rpi_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &rpi_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "POP", "rpi_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
  }
  return(fCC);
}

//
//  Function:   rpi_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "POP", "rpi_ReceiveSignalTerm()");
   //
   fRpiRunning = FALSE;
}

//
//  Function:   rpi_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "POP", "rpi_ReceiveSignalInt()");
   //
   fRpiRunning = FALSE;
}

//
//  Function:   rpi_ReceiveSignalUser1
//  Purpose:    SIGUSR1 signal from the RCU key handler
//
//  Parms:
//  Returns:    
//
static void rpi_ReceiveSignalUser1(int iSignal)
{
   if( sem_post(&tSemKey) == -1) LOG_Report(errno, "POP", "rpi_ReceiveSignalUser1():sem_post");
   PRINTF1("rpi_ReceiveSignalUser1():%d)" CRLF, iSignal);
   //LOG_Report(0, "POP", "rpi_ReceiveSignalUser1()");
}

//
//  Function:  rpi_WaitForKey
//  Purpose:   Wait for a new FP key (SIGUSR1)
//
//  Parms:     Timeout
//  Returns:   -1 if error
//  Note:      sem_init(pSem, 0, 0) : init sem, 0=local, sem-value=0
//             sem_wait(pSem))      : decrements the sem-value and waits until it is zero again 
//             sem_post(pSem)       : increments the sem-value
//             sem_destroy(pSem)    : clean up sem
//
static int rpi_WaitForKey(int iSecsTimeout)
{
   int             iCc;
   struct timespec stTimeout;

   clock_gettime(CLOCK_REALTIME, &stTimeout);
   stTimeout.tv_sec  += iSecsTimeout;
   //
   while ((iCc = sem_timedwait(&tSemKey, &stTimeout)) == -1 && errno == EINTR)
   {
      //PRINTF3("rpi_WaitForKey(): iCc=%d, errno=%d (%s)" CRLF, iCc, errno, strerror(errno));
      continue;       /* Restart if interrupted by handler */ 
   }
   //
   return(iCc);
}


#ifdef FEATURE_DUMP_DATE
//
//  Function:  rpi_Dump
//  Purpose:   Dump the buffer
//
//  Parms:     Buffer, size, hexmode
//  Returns:   
//
static void rpi_Dump(char *pcBuffer, int iSize, bool fHex)
{
   if(fHex)
   {
      int i;

      for(i=0; i<iSize;i++)
      {
         if( (i % 16) == 0) ES_printf(CRLF);
         ES_printf("%02x ", pcBuffer[i]);
      }
   }
   else
   {
      // pcBuffer[iSize-1] = 0;
      // ES_printf("%s", pcBuffer);
      while(iSize--) ES_printf("%c", *pcBuffer++);
   }
}
#endif   //FEATURE_DUMP_DATE
